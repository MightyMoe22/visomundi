namespace Beyond
{
    /// <inheritdoc />
    public class PickupPickAxe : PickupItem
    {
        protected override bool ItemPickedUp
        {
            get => GameState.PickAxe;
            set
            {
                GameState.PickAxe = value;
                GameState.NotifyInventory();
            }
        }
    }
}