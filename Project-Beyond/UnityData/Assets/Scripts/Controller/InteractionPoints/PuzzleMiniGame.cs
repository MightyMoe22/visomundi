namespace Beyond {
    /// <summary>
    /// Loads the Puzzle Minigame Scene
    /// </summary>
    public class PuzzleMiniGame : ParallaxInteractionPoint
    {
        public override Scenes.Side EntryPointFor => Scenes.Side.BackRight;
        public override void DoInteraction(ParallaxController controller) => Scenes.Go(Scenes.Side.BackRight);

    }
}