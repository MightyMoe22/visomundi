namespace Beyond {
    /// <inheritdoc />
    public class PickupChisel : PickupItem
    {
        protected override bool ItemPickedUp
        {
            get => GameState.Chisel;
            set { GameState.Chisel = value; GameState.NotifyInventory(); }
        }
    }
}