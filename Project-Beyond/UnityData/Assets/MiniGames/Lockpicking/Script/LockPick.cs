﻿using UnityEngine;

namespace Beyond
{
    /// <summary>
    /// Maintaining the lockpick-angle by observeing the mouse-input and converted
    /// it into valid values
    /// </summary>
    public class LockPick : MonoBehaviour
    {
        public float rotationSpeed = 10.0f;

        private float _rightMaxAngle = -90;
        private float _leftMaxAngle = 90;
        private Vector2 _lastPosition = new Vector2(0,0);
        public AudioSource lockpickSound;

        private bool _rotating;
        public bool Rotating
        {
            get => _rotating;
            set
            {
                if (value == _rotating) return;
                _rotating = value;
                if (value) lockpickSound.Play();
                else lockpickSound.Stop();
            }
        }
        private void Update()
        {
            FollowMouse();
        }

        private void FollowMouse()
        {

            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if(mousePosition != _lastPosition)
            {
                _lastPosition = mousePosition;
                Vector2 direction = new Vector2(mousePosition.x - transform.position.x, mousePosition.y - transform.position.y);
                Rotating = true;
                transform.up = direction;
                ClampRotation();
            }
            else
                Rotating = false;

        }

        private void ClampRotation()
        {
            if (transform.rotation.z > Quaternion.Euler(0, 0, _leftMaxAngle).z)
            {
                transform.rotation = Quaternion.Euler(0, 0, _leftMaxAngle);
            }
            else if (transform.rotation.z < Quaternion.Euler(0, 0, _rightMaxAngle).z)
            {
                transform.rotation = Quaternion.Euler(0, 0, _rightMaxAngle);
            }
        }
    }
}