using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.Serialization;

namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// Allows the Player to walk front/back and left/right and interact with interactions points in Parallax Scenes
    /// 
    /// Player Movement is also applied to "ParallaxLayers" see field "MovedLayersWithIncreasingDepth"
    /// </summary>
    [ExecuteInEditMode]
    public sealed class ParallaxController : MonoBehaviour
    {
        #region Unity Inspector Variables

        [Header("References")] [FormerlySerializedAs("MovedLayers")]
        public GameObject[] MovedLayersWithIncreasingDepth;

        [Range(-0.6f, 0)] public float ScaleSlope = -0.5f;
        public Player Player;
        public GameObject Effects;
        public GameObject Foreground;

        [Header("Parameters")] public int MaxPlayerLayer = 2;
        [Range(-1f, 1f)] public float MinX = -1;
        [Range(-1f, 1f)] public float MaxX = 1;
        public float PlayerSpeed = 0.4f;
        public int FramesForLayerChange = 20;

        [Header("Player Position")] public int PlayerLayer;
        [Range(-1f, 1f)]public float PlayerX;

        #endregion

        #region Setup

        private float _halfWidth;
        private int _currentPlayerLayer;
        private bool _disabled;
        private ParallaxInteractionPoint[] _interactions;


        private void Awake()
        {
            if (_disabled) return;
            // ReSharper disable once Unity.InefficientPropertyAccess
            // ReSharper disable once PossibleNullReferenceException
            _halfWidth = Camera.main.orthographicSize * Camera.main.aspect;

            GameStartPosition startPosition = GetComponent<GameStartPosition>();
            PlayerX = startPosition ? startPosition.PlayerX : 0;
            PlayerLayer = startPosition ? startPosition.PlayerLayer : 0;
            Player.Direction = startPosition ? startPosition.WalkingDirection : Player.WalkingDirection.Idle;
            _interactions = GetComponents<ParallaxInteractionPoint>();
            ParallaxInteractionPoint entryPoint =
                _interactions?.FirstOrDefault(point => point.EntryPointFor == Scenes.EntrySide);
            if (entryPoint != null) {
                PlayerX = entryPoint.PlayerX;
                PlayerLayer = entryPoint.PlayerLayer;
            }

            AssertInRange(ref MaxPlayerLayer, 0, MovedLayersWithIncreasingDepth?.Length ?? 0);
            ApplyScalingInstant();
        }

        #endregion

        private void Update()
        {
            if (_disabled) {
                ApplyPlayerX();
                if (PlayerLayer != _currentPlayerLayer) ApplyScalingInstant();
                return;
            }

            Player.Interaction = false;
            if (_interactions != null)
                foreach (ParallaxInteractionPoint interactionPoint in _interactions) {
                    interactionPoint.Possible = false;
                    if (!interactionPoint.InteractionPossible(PlayerLayer, PlayerX)) continue;

                    Player.Interaction = true;
                    interactionPoint.Possible = true;
                    
                    if (interactionPoint.PressedThisFrame
                        ? !Input.GetKeyDown(interactionPoint.KeyCode)
                        : !Input.GetKey(interactionPoint.KeyCode)) continue;
                    interactionPoint.DoInteraction(this);
                    return;
                }

            PlayerLayer += (Input.GetKey(KeyCode.UpArrow) ? 1 : 0) + (Input.GetKey(KeyCode.DownArrow) ? -1 : 0);
            if (PlayerLayer != _currentPlayerLayer) {
                ApplyLayerChange();
                return;
            }


            int moveX = (Input.GetKey(KeyCode.RightArrow) ? 1 : 0) + (Input.GetKey(KeyCode.LeftArrow) ? -1 : 0);
            SetPlayerDirection(moveX);
            PlayerX += moveX * Time.deltaTime * PlayerSpeed;
            ApplyPlayerX();
        }

        private void ApplyPlayerX()
        {
            AssertInRange(ref PlayerX, MinX, MaxX);
            float playerX = _halfWidth * PlayerX;
            SetX(Player.gameObject, playerX);
            SetX(Foreground, -2 * playerX);
            SetX(Effects, -2 * playerX);
            foreach (GameObject layer in MovedLayersWithIncreasingDepth) {
                if (!layer.activeSelf) continue;
                SetX(layer, -playerX * (3 * layer.transform.localScale.x - 1));
            }
        }

        private void SetPlayerDirection(int moveX) =>
            Player.Direction = moveX == 0 ? Player.WalkingDirection.Idle :
                moveX == 1 ? Player.WalkingDirection.Right : Player.WalkingDirection.Left;


        #region Layer Scaling

        private void ApplyLayerChange()
        {
            AssertInRange(ref PlayerLayer, 0, MaxPlayerLayer);
            if (PlayerLayer == _currentPlayerLayer || _disabled || MovedLayersWithIncreasingDepth == null) return;

            int offset = PlayerLayer - _currentPlayerLayer;
            _currentPlayerLayer = PlayerLayer;
            if (Application.isEditor && !Application.IsPlaying(this)) ApplyScalingInstant();
            else StartCoroutine(RescaleLayers(offset));
        }

        private IEnumerator RescaleLayers(int initialOffset)
        {
            _disabled = true;
            Player.Direction = initialOffset < 0 ? Player.WalkingDirection.Front : Player.WalkingDirection.Back;

            // make all necessary layers visible
            for (int i = _currentPlayerLayer; i < MovedLayersWithIncreasingDepth.Length; i++)
                MovedLayersWithIncreasingDepth[i].SetActive(true);
            Foreground.SetActive(_currentPlayerLayer == 0);

            int alphaLayer = _currentPlayerLayer - (initialOffset > 0 ? 1 : 0);
            SpriteRenderer[] alphasForeground = alphaLayer == 0
                ? Foreground.GetComponentsInChildren<SpriteRenderer>()
                : new SpriteRenderer[0];
            SpriteRenderer[] alphas = MovedLayersWithIncreasingDepth[alphaLayer]
                .GetComponentsInChildren<SpriteRenderer>();

            //scale layers
            float offsetX = _halfWidth * PlayerX;
            for (float offsetPercentage = 1; offsetPercentage >= 0; offsetPercentage -= 1f / FramesForLayerChange) {
                float scaleOffset = offsetPercentage * initialOffset;
                for (int i = 0; i < MovedLayersWithIncreasingDepth.Length; i++) {
                    float scale = 1 + (scaleOffset + i - _currentPlayerLayer) * ScaleSlope;
                    MovedLayersWithIncreasingDepth[i].transform.localScale = Vector3.one * scale;
                    SetX(MovedLayersWithIncreasingDepth[i], -offsetX * (3 * scale - 1));
                }

                float alpha = initialOffset < 0 ? 1 - offsetPercentage : offsetPercentage;
                foreach (SpriteRenderer spriteRenderer in alphas) SetAlpha(spriteRenderer, alpha);
                foreach (SpriteRenderer spriteRenderer in alphasForeground) SetAlpha(spriteRenderer, alpha);

                if (offsetPercentage > 0)
                    yield return null;
            }

            float alphaEnd = initialOffset < 0 ? 1 : 0;
            foreach (SpriteRenderer spriteRenderer in alphas) SetAlpha(spriteRenderer, alphaEnd);
            foreach (SpriteRenderer spriteRenderer in alphasForeground) SetAlpha(spriteRenderer, alphaEnd);

            // hide unnecessary Layers
            for (int i = 0; i < _currentPlayerLayer; i++)
                MovedLayersWithIncreasingDepth[i].SetActive(false);
            Foreground.SetActive(_currentPlayerLayer == 0);

            Player.Direction = Player.WalkingDirection.Idle;
            _disabled = false;
        }

        private static void SetAlpha(SpriteRenderer spriteRenderer, float alpha)
        {
            Color color = spriteRenderer.color;
            color.a = alpha;
            spriteRenderer.color = color;
        }

        private void ApplyScalingInstant()
        {
            _currentPlayerLayer = PlayerLayer;
            if (MovedLayersWithIncreasingDepth == null) return;
            for (int i = 0; i < MovedLayersWithIncreasingDepth.Length; i++) {
                MovedLayersWithIncreasingDepth[i].SetActive(i >= _currentPlayerLayer);
                MovedLayersWithIncreasingDepth[i].transform.localScale =
                    Vector3.one * (1 + (i - _currentPlayerLayer) * ScaleSlope);
            }

            Foreground.SetActive(_currentPlayerLayer == 0);
        }

        #endregion

        #region Helpers

        private static void AssertInRange<T>(ref T value, T lowerBound, T upperBound) where T : IComparable<T>
        {
            if (value.CompareTo(lowerBound) < 0) value = lowerBound;
            else if (value.CompareTo(upperBound) > 0) value = upperBound;
        }

        private static void SetX(GameObject sprite, float playerX)
        {
            if (!sprite) return;
            sprite.transform.position = new Vector3(playerX, sprite.transform.position.y);
        }

        #endregion

        public void SetActive(bool active)
        {
            _disabled = !active;
            if (!active) Player.Direction = Player.WalkingDirection.Idle;
        }
    }
}