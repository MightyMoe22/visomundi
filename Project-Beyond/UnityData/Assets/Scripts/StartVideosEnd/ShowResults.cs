using UnityEngine;
using UnityEngine.SceneManagement;

namespace Beyond
{
    /// <summary>
    /// Shows the correct resultimage
    /// </summary>
    public class ShowResults : MonoBehaviour
    {
        public GameObject Tonne, Desert, Space, Winter, Four;

        private void Awake()
        {
            Tonne.SetActive(GameState.Choice == GameState.Result.Tonne);
            Desert.SetActive(GameState.Choice == GameState.Result.Desert);
            Space.SetActive(GameState.Choice == GameState.Result.Space);
            Winter.SetActive(GameState.Choice == GameState.Result.Winter);
            Four.SetActive(GameState.Choice == GameState.Result.Four);
            
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.R)) SceneManager.LoadScene(Scenes.Instance.Main);
        }
    }
}