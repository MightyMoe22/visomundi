namespace Beyond
{
    /// <inheritdoc />
    public class PickupWinterAmulet : PickupItem
    {
        protected override bool ItemPickedUp
        {
            get => GameState.WinterAmulet;
            set
            {
                GameState.WinterAmulet = value;
                GameState.NotifyInventory();
            }
        }

        protected override bool AdditionalRequirement() => base.AdditionalRequirement() && GameState.StoneOpened;

        protected override void SaySth(ParallaxController controller) =>
            controller.Player.SpeechBubble.Say(Amulets.GetNextText(), Amulets.GetClip());
    }
}