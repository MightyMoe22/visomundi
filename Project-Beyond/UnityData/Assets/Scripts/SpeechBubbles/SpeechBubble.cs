using UnityEngine;
using UnityEngine.UI;

namespace Beyond
{
    /// <summary>
    /// Shows the Speechbubble for a given time with a specific text
    /// If an audioclip is provided the clip is also played
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public class SpeechBubble : MonoBehaviour
    {
        public Text TextScript;
        public GameObject HideThis;
        public float DefaultVisibleTime = 4;

        [Header("Speech Indikator")] public GameObject SpeechIndicator;
        public bool ShowSpeechIndicatorWhenNotTalking;

        private AudioSource _audioSource;

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
            Hide();
        }

        internal void Say(string text, AudioClip clip)
        {
            Say(text, clip, clip != null ? clip.length : DefaultVisibleTime);
        }

        internal void Say(string text, AudioClip clip, float hideAfter)
        {
            SpeechIndicator.SetActive(false);
            HideThis.SetActive(true);
            TextScript.text = text;
            _audioSource.clip = clip;
            _audioSource.Play();
            CancelInvoke(nameof(Hide));
            if (hideAfter > 0) Invoke(nameof(Hide), hideAfter);
        }

        internal void Hide()
        {
            HideThis.SetActive(false);
            SpeechIndicator.SetActive(ShowSpeechIndicatorWhenNotTalking);
        }
    }
}