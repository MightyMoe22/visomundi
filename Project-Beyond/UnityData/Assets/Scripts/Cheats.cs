using UnityEngine;
using UnityEngine.SceneManagement;

namespace Beyond
{
    /// <summary>
    /// lots of cheats!!!
    /// </summary>
    public class Cheats : MonoBehaviour
    {
        private static bool _cheated;

        private void Awake()
        {
            if (_cheated) return;
            Timer.SetValue(1200, Time.time);
            _cheated = true;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.R)) SceneManager.LoadScene(Scenes.Instance.Main);
            if (Input.GetKeyDown(KeyCode.P)) GameState.CheatsEnabled = !GameState.CheatsEnabled;

            if (!GameState.CheatsEnabled) return;

            if (Input.GetKeyDown(KeyCode.T))
                if (Timer.Paused) {
                    Timer.Resume(Time.time);
                    Timer.SetValue(900, Time.time);
                } else Timer.Pause(Time.time);

            if (Input.GetKeyDown(KeyCode.A)) {
                Rotate(ref GameState.DesertAmulet, ref GameState.WinterAmulet, ref GameState.SpaceAmulet);
                GameState.NotifyInventory();
            }

            if (Input.GetKeyDown(KeyCode.I)) {
                Rotate(ref GameState.PickAxe, ref GameState.BunsenBrenner, ref GameState.Chisel);
                GameState.NotifyInventory();
            }

            if (Input.GetKeyDown(KeyCode.Q)) {
                RotateQuiz();
                GameState.NotifyInventory();
            }

            if (Input.GetKeyDown(KeyCode.Alpha1)) SceneManager.LoadScene(Scenes.Instance.Pantheon1);
            if (Input.GetKeyDown(KeyCode.Alpha2)) SceneManager.LoadScene(Scenes.Instance.Desert1);
            if (Input.GetKeyDown(KeyCode.Alpha3)) SceneManager.LoadScene(Scenes.Instance.Space1);
            if (Input.GetKeyDown(KeyCode.Alpha4)) SceneManager.LoadScene(Scenes.Instance.Winter1);

            if (Input.GetKeyDown(KeyCode.L)) Timer.SetValue(0, Time.time);
            if (Input.GetKeyDown(KeyCode.S)) GameState.WelcomeClipPlayed = true;
        }

        private static void Rotate(ref bool one, ref bool two, ref bool three)
        {
            int state = (one ? 1 : 0) + (two ? 2 : 0) + (three ? 4 : 0);
            state = ++state % 8;
            one = (state & 1) > 0;
            two = (state & 2) > 0;
            three = (state & 4) > 0;
        }

        private static void RotateQuiz()
        {
            int state = (GameState.DesertQuizAllowed ? GameState.DesertQuizSucceeded ? 2 : 1 : 0) +
                        (GameState.WinterQuizAllowed ? GameState.WinterQuizSucceeded ? 6 : 3 : 0) +
                        (GameState.SpaceQuizAllowed ? GameState.SpaceQuizSucceeded ? 18 : 9 : 0);
            state = ++state % 27;
            GameState.DesertQuizSucceeded = state % 3 == 2;
            GameState.DesertQuizAllowed = state % 3 > 0;
            state /= 3;
            GameState.WinterQuizSucceeded = state % 3 == 2;
            GameState.WinterQuizAllowed = state % 3 > 0;
            state /= 3;
            GameState.SpaceQuizSucceeded = state % 3 == 2;
            GameState.SpaceQuizAllowed = state % 3 > 0;
        }
    }
}