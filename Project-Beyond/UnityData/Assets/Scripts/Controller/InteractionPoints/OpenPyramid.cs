using UnityEngine;

namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// Opens the Pyramid if the Player has already picked up the Pickaye in winter2
    /// </summary>
    public class OpenPyramid : ParallaxInteractionPoint
    {
        public string NoPickaxeText;
        public GameObject ClosedPyramidSprite;
        public AudioClip OpenClip1, OpenClip2;
        private ParallaxController _parallaxController;

        private void Awake() => ClosedPyramidSprite.SetActive(!GameState.PyramidOpened);

        public override void DoInteraction(ParallaxController controller)
        {
            _parallaxController = controller;
            if (GameState.PickAxe) {
                ClosedPyramidSprite.SetActive(false);
                GameState.PyramidOpened = true;
                AudioSource.PlayClipAtPoint(OpenClip1, _parallaxController.Player.transform.position);
                Invoke(nameof(SecondClip), OpenClip1.length);
            } else _parallaxController.Player.SpeechBubble.Say(NoPickaxeText, null);
        }

        private void SecondClip() =>
            AudioSource.PlayClipAtPoint(OpenClip2, _parallaxController.Player.transform.position);

        protected override bool AdditionalRequirement() => !GameState.PyramidOpened;
    }
}