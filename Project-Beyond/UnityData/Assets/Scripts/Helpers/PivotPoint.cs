using System;
using UnityEngine;

namespace Beyond
{
    [ExecuteInEditMode]
    public class PivotPoint : MonoBehaviour
    {
        private float _currentY;
        public float YOffset;

        private void Awake()
        {
            _currentY = YOffset = transform.localPosition.y;
        }

        private void Update()
        {
            if (Math.Abs(_currentY - transform.localPosition.y) > 0.01f) {
                // moved gameObject
                _currentY = YOffset = transform.localPosition.y;
                return;
            }

            float offset = YOffset - _currentY;
            if (Math.Abs(offset) < 0.01f) return;
            transform.Translate(0, offset, 0, Space.Self);
            foreach (Transform child in transform) child.Translate(0, -offset, 0, Space.Self);
        }
    }
}