﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Beyond
{
    /// <summary>
    /// Generates the values needed for the game and checks if angle of the lockpick is right
    /// </summary>
    public class Lock : MonoBehaviour
    {
        public float toleranceRange = 5.0f;
        public float rotationSpeed = 100.0f;
        public LockPick lockPick;
        public bool locked = true;

        public float _goalAngle;
        public float _rightToleranceAngle;
        public float _leftToleranceAngle;

        public AudioSource _rightAngle;
        public AudioSource _unlocked;
        public AudioSource _turnLock;

        // Start is called before the first frame update
        private void Start()
        {
            GeneratedLockValues();
        }

        // Update is called once per frame
        private void Update()
        {
            if (locked)
            {
                TryRotateLock();
            }
        }

        private void GeneratedLockValues()
        {
            _goalAngle = UnityEngine.Random.Range(0.0f + toleranceRange, 180.0f - toleranceRange);
            _rightToleranceAngle = _goalAngle + toleranceRange;
            _leftToleranceAngle = _goalAngle - toleranceRange;
            Debug.Log("GoalAngle: " + _goalAngle + "left-Z: " + _leftToleranceAngle + " right-Z: " + _rightToleranceAngle);
        }

        private void TryRotateLock()
        {
            if (CheckLockPickAngle())
            {
                RotateLock();
            }
            else
            {
                ResetLockRotation();
            }
        }

        private void ResetLockRotation()
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }

        private bool CheckLockPickAngle()
        {
            bool isInPossition = false;
            float lockPickAngle = NormalizeEuler(lockPick.transform.rotation.eulerAngles.z);

            if (_leftToleranceAngle <= lockPickAngle && _rightToleranceAngle >= lockPickAngle)
            {
                isInPossition = true;
                _rightAngle.Play();
            }

            return isInPossition;
        }

        private static float NormalizeEuler(float eulerAngle)
        {
            if (eulerAngle > 90)
            {
                eulerAngle -= 360;
            }
            return (eulerAngle - 90.0f) * -1;
        }

        private void RotateLock()
        {
            if (Input.GetKey(KeyCode.RightArrow))
            {
                _turnLock.Play();
                float rotation = -1 * rotationSpeed;
                rotation *= Time.deltaTime;
                transform.Rotate(0, 0, rotation);
                CheckIfUnlocked();
            }
            _turnLock.Stop();
        }

        private void CheckIfUnlocked()
        {
            if (NormalizeEuler(transform.rotation.eulerAngles.z) >= 180)
            {
                locked = false;
                _unlocked.Play();
                GameState.SpaceAmuletAvailable = true;
                Scenes.Go(Scenes.Side.Front);
            }
        }
    }
}
