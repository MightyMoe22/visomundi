using System;
using UnityEngine;
using UnityEngine.UI;

namespace Beyond
{
    /// <summary>
    /// Triggers an Event when clicked
    /// </summary>
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(BoxCollider2D))]
    public class QuizButton : MonoBehaviour
    {
        public event Action ButtonPressed;
        private Color _defaultColor = Color.white;
        internal Color HighlightColor = Color.white;

        private void Awake() => ButtonPressed += () =>
            AudioSource.PlayClipAtPoint(SoundEffects.Instance.QuizButton, transform.position);

        private void OnMouseEnter() => SetColor(HighlightColor);
        private void OnMouseDown() => ButtonPressed?.Invoke();
        private void OnMouseUp() => SetColor(_defaultColor);
        private void OnMouseExit() => SetColor(_defaultColor);

        private void SetColor(Color color)
        {
            GetComponent<SpriteRenderer>().color = color;
            Text text = GetComponentInChildren<Text>();
            if (text != null) text.color = color;
        }

        public void SetText(string text)
        {
            Text textScript = GetComponentInChildren<Text>();
            if (textScript != null) textScript.text = text;
        }

        public void SetDefaultColor(Color color)
        {
            _defaultColor = color;
            SetColor(color);
        }
    }
}