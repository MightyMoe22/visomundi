using System;
using UnityEngine;

namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// Allows the Player to walk right/left in Pyramid / Moonbase scene
    /// </summary>
    [ExecuteInEditMode]
    public sealed class BuildingController : MonoBehaviour
    {
        [Header("References")] public Player Player;
        public GameObject Building;
        public GameObject Item;

        [Header("Parameters")] public float PlayerSpeed = 0.7f;
        public float BuildingOffsetMultiplier = 0.4f;

        [Header("Player Position")] [Range(-1f, 1f)]
        public float PlayerX;

        private float _halfWidth;

        private void Awake()
        {
            // ReSharper disable once Unity.InefficientPropertyAccess
            // ReSharper disable once PossibleNullReferenceException
            _halfWidth = Camera.main.orthographicSize * Camera.main.aspect;
            PlayerX = Scenes.EntrySide == Scenes.Side.Back ? GetComponent<SpaceBaseInteractions>().Lock : -0.95f;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape) || PlayerX < -0.95f && Input.GetKey(KeyCode.LeftArrow))
                Scenes.Go(Scenes.Side.Left);

            int moveX = (Input.GetKey(KeyCode.RightArrow) ? 1 : 0) + (Input.GetKey(KeyCode.LeftArrow) ? -1 : 0);
            PlayerX += moveX * Time.deltaTime * PlayerSpeed;
            AssertInRange(ref PlayerX, -1, 1);

            Player.Direction = moveX == 0 ? Player.WalkingDirection.Idle :
                moveX == 1 ? Player.WalkingDirection.Right : Player.WalkingDirection.Left;

            float playerX = _halfWidth * PlayerX;
            SetX(Player.gameObject, playerX);
            SetX(Building, -playerX * BuildingOffsetMultiplier);
        }

        #region Helpers

        private static void AssertInRange<T>(ref T value, T lowerBound, T upperBound) where T : IComparable<T>
        {
            if (value.CompareTo(lowerBound) < 0) value = lowerBound;
            else if (value.CompareTo(upperBound) > 0) value = upperBound;
        }

        private static void SetX(GameObject sprite, float playerX)
        {
            if (!sprite) return;
            sprite.transform.position = new Vector3(playerX, sprite.transform.position.y);
        }

        #endregion
    }
}