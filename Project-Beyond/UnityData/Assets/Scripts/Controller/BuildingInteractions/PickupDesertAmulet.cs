﻿using System;
using UnityEngine;

namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// "Picking up the Desert-Amulet"-Interaction
    ///
    /// used in the Pyramid scene
    /// </summary>
    [RequireComponent(typeof(BuildingController))]
    [ExecuteAlways]
    public class PickupDesertAmulet : MonoBehaviour
    {
        [Range(-1, 1)] public float PlayerX;
        [Range(0, 1)] public float PlayerXRange;
        public GameObject Amulet;

        private BuildingController _buildingController;

        private void Awake()
        {
            _buildingController = GetComponent<BuildingController>();
            Amulet.SetActive(!GameState.DesertAmulet);
        }

        private void Update()
        {
            if (Math.Abs(_buildingController.PlayerX - PlayerX) < PlayerXRange && !GameState.DesertAmulet) {
                _buildingController.Player.Interaction = true;
                if (!Input.GetKeyDown(KeyCode.Space)) return;
                
                _buildingController.Player.SpeechBubble.Say(Amulets.GetNextText(), Amulets.GetClip());
                AudioSource.PlayClipAtPoint(SoundEffects.Instance.PickupItem, Amulet.transform.position);
                Amulet.SetActive(false);
                GameState.DesertAmulet = true;
                GameState.NotifyInventory();
            } else _buildingController.Player.Interaction = false;
        }
    }
}