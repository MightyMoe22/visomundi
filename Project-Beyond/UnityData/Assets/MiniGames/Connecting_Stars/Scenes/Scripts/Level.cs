﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Level verwaltet das konkrete Level, welches gerade gespielt wird
public class Level : MonoBehaviour
{
    public int starcount;
    public int maxClicks;
    public Sprite levelSprite;

    private int currentStarNumber;
    private GameObject currentStarObject;
    private LineManager lineManager;
    private LevelManager levelManager;
    private AudioManager audioManager;
    


    // Start is called before the first frame update
    void Start()
    {
        lineManager = FindObjectOfType<LineManager>();
        levelManager = FindObjectOfType<LevelManager>();
        audioManager = FindObjectOfType<AudioManager>();
        Debug.Log(levelManager);
        ResetLevel();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    //überprüft ob richtiger Stern ausgewählt und steuert entsprechend andere Objekte
    public void starSelected(GameObject star)
    {
        if(!LevelManager.gameStarted)
        {
            return;
        }

        Star StarScript = star.GetComponent<Star>();

        if (numberValid(StarScript))
        {

            if (currentStarObject != null)
            {
                
                lineManager.DrawLine(currentStarObject.transform.position, star.transform.position);

            }

            currentStarObject = star;
            currentStarNumber++;

            StarScript.onHit(true);
            if(levelManager == null)
            {
                levelManager = FindObjectOfType<LevelManager>();
            }
            
            levelManager.validStarSelected();
            audioManager.SternSound(currentStarNumber);
           
        }
        else
        {
            StarScript.onHit(false);
        }

    }


    //überprüft Nummern des Sterns mit dem Zähler
    private bool numberValid(Star starScript)
    {
        foreach(int number in starScript.numberInOrder)
        {
          
            if(number - 1 == currentStarNumber)
            {
              
                return true;
            }
        }

        return false;
    }


    //Reset
    public void ResetLevel()
    {
      
        currentStarNumber = 0;
        currentStarObject = null;
    }

}
