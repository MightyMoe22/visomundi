using UnityEngine;

namespace Beyond {
    /// <inheritdoc />
    /// <summary>
    /// Tells "Scenes" to load the scene placed at the back of the current scene
    /// </summary>
    public class GoToBackScene : ParallaxInteractionPoint
    {
        public override KeyCode KeyCode => KeyCode.UpArrow;
        public override bool PressedThisFrame => true;
        public override Scenes.Side EntryPointFor => Scenes.Side.Back;
        public override void DoInteraction(ParallaxController controller) => Scenes.Go(Scenes.Side.Back);
    }
}