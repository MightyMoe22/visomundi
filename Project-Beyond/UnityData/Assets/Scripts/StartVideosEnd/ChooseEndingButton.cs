using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Beyond
{
    /// <summary>
    /// shows the small red cross when hovering over t
    /// loads the corresponding ending, when the button is clicked
    /// </summary>
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(BoxCollider2D))]
    public class ChooseEndingButton : MonoBehaviour
    {
        public Color HighlightColor;
        public GameObject Cross;

        public enum Endings
        {
            Desert, Space, Winter
        }

        public Endings Ending;
        
        private SpriteRenderer _sprite;

        private void Awake()
        {
            Cross.SetActive(false);
            _sprite = GetComponent<SpriteRenderer>();
        }

        private void OnMouseEnter()
        {
            Cross.SetActive(true);
            _sprite.color = HighlightColor;
        }

        private void OnMouseExit()
        {
            Cross.SetActive(false);
            _sprite.color = Color.white;
        }

        private void OnMouseDown() {
            Timer.Pause(Time.time);
            switch (Ending) {
                case Endings.Desert:
                    GameState.Choice = GameState.Result.Desert;
                    SceneManager.LoadScene(Scenes.Instance.EndingDesert);
                    break;
                case Endings.Space:
                    GameState.Choice = GameState.Result.Space;
                    SceneManager.LoadScene(Scenes.Instance.EndingSpace);
                    break;
                case Endings.Winter:
                    GameState.Choice = GameState.Result.Winter;
                    SceneManager.LoadScene(Scenes.Instance.EndingWinter);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}