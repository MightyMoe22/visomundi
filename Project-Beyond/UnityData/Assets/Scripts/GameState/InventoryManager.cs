using UnityEngine;
using UnityEngine.Serialization;

namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// Shows the corresponding sprites
    /// </summary>
    public class InventoryManager : MonoBehaviour
    {
        [Header("Items")] public GameObject PickAxe;

        [FormerlySerializedAs("ScrewDriver")] [FormerlySerializedAs("SomeItemNeededInSpace")]
        public GameObject BunsenBrenner;

        [FormerlySerializedAs("Wrench")] [FormerlySerializedAs("Screwdriver")] public GameObject ArcheologyTool;

        [Header("Amulets")] public GameObject DesertAmulet;
        public GameObject SpaceAmulet;
        public GameObject WinterAmulet;

        [Header("Permits")] public GameObject CubePermit;
        public GameObject SpacePermit;
        public GameObject FlatPermit;

        private void Awake()
        {
            GameState.ItemsChanged += ApplyItems;
            ApplyItems();
        }

        private void ApplyItems()
        {
            if (PickAxe != null) PickAxe.SetActive(GameState.PickAxe);
            if (BunsenBrenner != null) BunsenBrenner.SetActive(GameState.BunsenBrenner);
            if (ArcheologyTool != null) ArcheologyTool.SetActive(GameState.Chisel);

            if (DesertAmulet != null) DesertAmulet.SetActive(GameState.DesertAmulet);
            if (SpaceAmulet != null) SpaceAmulet.SetActive(GameState.SpaceAmulet);
            if (WinterAmulet != null) WinterAmulet.SetActive(GameState.WinterAmulet);

            if (CubePermit != null) SetVisible(CubePermit, GameState.DesertQuizSucceeded, GameState.DesertQuizAllowed);
            if (SpacePermit != null) SetVisible(SpacePermit, GameState.SpaceQuizSucceeded, GameState.SpaceQuizAllowed);
            if (FlatPermit != null) SetVisible(FlatPermit, GameState.WinterQuizSucceeded, GameState.WinterQuizAllowed);
        }

        private static void SetVisible(GameObject go, bool hundredPercent, bool fiftyPercent)
        {
            go.SetActive(hundredPercent || fiftyPercent);
            SetAlpha(go, hundredPercent ? 1 : 0.5f);
        }

        private static void SetAlpha(GameObject go, float alpha)
        {
            SpriteRenderer spriteRenderer = go.GetComponent<SpriteRenderer>();
            if (!spriteRenderer) return;
            Color color = spriteRenderer.color;
            color.a = alpha;
            spriteRenderer.color = color;
        }
    }
}