﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Beyond
{
    /// <summary>
    /// starts the intro-video
    /// </summary>
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(BoxCollider2D))]
    public class StartButton : MonoBehaviour
    {
        public Color HighlightColor;
        private SpriteRenderer _sprite;

        private void Awake()
        {
            GameState.ResetVariables();
            _sprite = GetComponent<SpriteRenderer>();
        }

        private void OnMouseEnter() => _sprite.color = HighlightColor;
        private void OnMouseExit() => _sprite.color = Color.white;
        private void OnMouseDown() => SceneManager.LoadScene(Scenes.Instance.IntroVideo);

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space)) SceneManager.LoadScene(Scenes.Instance.IntroVideo);
            if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit();
        }
    }
}