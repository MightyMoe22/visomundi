﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/* InputManager steuert Input und überprüft, ob etwas getroffen wurde*/

public class InputManager : MonoBehaviour
{
    LineManager lineManager;
    private Level currentLevel;
    private LevelManager levelManager; 


    // Start is called before the first frame update
    void Start()
    {
        lineManager = FindObjectOfType<LineManager>();
        levelManager = FindObjectOfType<LevelManager>();
        currentLevel = levelManager.currentLevel;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0) && LevelManager.gameStarted)
        {
            mouseButtonClicked();
        }
    }



    //Bei Mausklick wird überpüft, ob ein Stern getroffen wurde und diese Info wird an das Level weitergegeben
    void mouseButtonClicked()
    {

        //Richtung vom Ray wird festgelegt
        Vector2 cubeRay = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        //Raycast wird erzeugt und Treffer wird als cubeHit gespeichert - cubeHit ist Null wenn nichts getroffen wird
        RaycastHit2D cubeHit = Physics2D.Raycast(cubeRay, Vector2.zero);

        //Wurde irgendwas getroffen...
        if (cubeHit.transform != null)
            {
                
                if (cubeHit.transform.tag == "Star" && levelManager.isClickAllowed())
                {
                   
                    Star star = cubeHit.transform.gameObject.GetComponent<Star>();

                    
                    currentLevel.starSelected(star.gameObject);

                    levelManager.decreaseClicks();
                }
                
            }
            else
            {
                Debug.Log("Nichts getroffen!");
            }
        
    }


  
    // Level wird neu zugewiesen
    public void LevelReset()
    {
        currentLevel = levelManager.currentLevel;
    }

}
