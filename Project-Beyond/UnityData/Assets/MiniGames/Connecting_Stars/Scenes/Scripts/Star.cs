﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Star legt fest wie ein Stern auf verschiedene Aktionen reagiert
public class Star : MonoBehaviour
{
    public int[] numberInOrder;
    public Color falseColor;
    public Sprite glowStar;


    // Start is called before the first frame update
    public Color selectedColor;

    void Start()
    {
       
            
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    //Sprite wird bei Klick verändert, wird vom Level aufgerufen 
    public void onHit(bool valid)
    {
    

        if (valid)
        {
            SpriteRenderer SRenderer = GetComponent<SpriteRenderer>();
            SRenderer.sprite = glowStar;
            //GetComponent<SpriteRenderer>().color = selectedColor;
        }
        else
        {
            StartCoroutine(falseHit());
        }
    }


    private IEnumerator falseHit()
    {
        SpriteRenderer SRenderer = GetComponent<SpriteRenderer>();
        Color tempColor = SRenderer.color;
        SRenderer.color = falseColor;

        yield return new WaitForSeconds(0.5f);
        SRenderer.color = tempColor;
    }
}
