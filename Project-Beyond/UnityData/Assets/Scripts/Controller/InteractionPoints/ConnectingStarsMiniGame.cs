using UnityEngine;

namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// effectivly loads the "Connecting Stars"-Minigame Scene when "space" is pressed
    /// </summary>
    public class ConnectingStarsMiniGame : GoToBackScene
    {
        public override KeyCode KeyCode => KeyCode.Space;
    }
}