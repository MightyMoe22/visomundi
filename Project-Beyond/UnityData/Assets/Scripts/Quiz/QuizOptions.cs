using System;
using UnityEngine;

namespace Beyond
{
    /// <summary>
    /// Collects 4 ButtonEvents into a single "optionChosen" event
    /// SetText sets all 4 button text in one methodcall
    /// </summary>
    public class QuizOptions : MonoBehaviour
    {
        public event Action<int> OptionChosen;
        
        public QuizButton Option1, Option2, Option3, Option4;

        private void Awake()
        {
            Option1.ButtonPressed += () => OptionChosen?.Invoke(1);
            Option2.ButtonPressed += () => OptionChosen?.Invoke(2);
            Option3.ButtonPressed += () => OptionChosen?.Invoke(3);
            Option4.ButtonPressed += () => OptionChosen?.Invoke(4);
        }

        public Color HighlightColor
        {
            set
            {
                Option1.HighlightColor = value;
                Option2.HighlightColor = value;
                Option3.HighlightColor = value;
                Option4.HighlightColor = value;
            }
        }

        public void SetText(QuizTexts.QuestionSet question)
        {
            Option1.SetText(question.Option1);
            Option2.SetText(question.Option2);
            Option3.SetText(question.Option3);
            Option4.SetText(question.Option4);
        }

        public void SetColor(int optionNumber, Color color)
        {
            if (optionNumber == 1) Option1.SetDefaultColor(color);
            else if (optionNumber == 2) Option2.SetDefaultColor(color);
            else if (optionNumber == 3) Option3.SetDefaultColor(color);
            else if (optionNumber == 4) Option4.SetDefaultColor(color);
            else throw new ArgumentException("option number out of range: " + optionNumber);
        }

        public void ResetColor()
        {
            Option1.SetDefaultColor(Color.white);
            Option2.SetDefaultColor(Color.white);
            Option3.SetDefaultColor(Color.white);
            Option4.SetDefaultColor(Color.white);
        }
    }
}