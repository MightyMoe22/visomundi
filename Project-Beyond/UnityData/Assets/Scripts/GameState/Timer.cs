namespace Beyond
{
    internal static class Timer
    {
        private static float _baseValue;
        private static float _referenceTime;
        internal static bool Paused { get; private set; }= true;

        public static float GetValue(float absoluteTime)
        {
            float value = _baseValue + (Paused ? 0 : _referenceTime - absoluteTime);
            return value;
        }

        public static void SetValue(float value, float absoluteTime)
        {
            _baseValue = value;
            _referenceTime = absoluteTime;
        }
        
        public static void Pause(float absoluteTime)
        {
            RecalculateReferenceTime(absoluteTime);
            Paused = true;
        }
        
        public static void Resume(float absoluteTime)
        {
            RecalculateReferenceTime(absoluteTime);
            Paused = false;
        }

        private static void RecalculateReferenceTime(float absoluteTime)
        {
            _baseValue = GetValue(absoluteTime);
            _referenceTime = absoluteTime;
        }
    }
}