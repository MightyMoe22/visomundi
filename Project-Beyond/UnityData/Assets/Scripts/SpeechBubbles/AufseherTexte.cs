using UnityEngine;

namespace Beyond
{
    public class AufseherTexte : MonoBehaviour
    {
        public string FourteenMinutes, Half, OneMinute;
        public SpeechBubble SpeechBubble;

        private void Awake()
        {
            TimerEvents.Nineteen += () =>
            {
                if (SpeechBubble != null) {
                    SpeechBubble.Say(FourteenMinutes, null, 10);
                }
            };
            TimerEvents.Half += () =>
            {
                if (SpeechBubble != null) SpeechBubble.Say(Half, null, 10);
            };
            TimerEvents.One += () =>
            {
                if (SpeechBubble != null) SpeechBubble.Say(OneMinute, null, 10);
            };
        }
    }
}