using System;
using UnityEngine;

namespace Beyond
{
    /// <summary>
    /// Singleton, that holds references to 3 speech bubble texts for picking up the first/second/third amulet
    /// </summary>
    [CreateAssetMenu]
    public class Amulets : ScriptableObject
    {
        [Multiline(2)] public string FirstAmuletText;
        public AudioClip FirstAmuletClip;
        [Multiline(2)] public string SecondAmuletText;
        public AudioClip SecondAmuletClip;
        [Multiline(2)] public string ThirdAmuletText;
        public AudioClip ThirdAmuletClip;

        private static Amulets _instance;

        private static Amulets Instance =>
            _instance != null ? _instance : _instance = Resources.Load<Amulets>("Amulets");

        public static string GetNextText()
        {
            GameState.AmuletsFound %= 3; //loop around during testing...
            GameState.AmuletsFound++;
            switch (GameState.AmuletsFound) {
                case 1: return Instance.FirstAmuletText;
                case 2: return Instance.SecondAmuletText;
                case 3: return Instance.ThirdAmuletText;
                default:
                    throw new ArgumentOutOfRangeException(nameof(GameState.AmuletsFound), "GetNextText() too often called!");
            }
        }


        public static AudioClip GetClip()
        {
            switch (GameState.AmuletsFound) {
                case 1: return Instance.FirstAmuletClip;
                case 2: return Instance.SecondAmuletClip;
                case 3: return Instance.ThirdAmuletClip;
                default:
                    throw new ArgumentOutOfRangeException(nameof(GameState.AmuletsFound), "GetNextText() not called before!");
            }
        }
    }
}