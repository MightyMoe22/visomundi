using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// Responsible for Choosing between 3 main endings: cube society, flat earther, roundies
    ///
    /// The Player can talk to the Overseer before the timer has reached 0
    /// or is forced into this choice when the timer elapses. (See method SubscribeToTimerElapsedEvent)
    /// 
    /// If the Player hasn't reached sufficent qualifications (complete any quiz)
    /// he is forced into the 4th ending: street
    /// 
    /// </summary>
    public class ChooseEnding : ParallaxInteractionPoint
    {
        public override void DoInteraction(ParallaxController controller)
        {
            if (GameState.NoChoiceAvailable) SpeechBubble.Say(NotYetAllowedText, null);
            else ShowChoice(true);
        }

        public string NotYetAllowedText;

        [Header("Ending Parameters")] public GameObject Options;
        [Range(-1, 1)] public float PlayerXWhileChoosing;
        public KeyCode CancelWith = KeyCode.Escape;
        [FormerlySerializedAs("GotoTonneEndingDelay")] public float GotoStreetEndingDelay = 10;

        [Header("Texts")] public SpeechBubble SpeechBubble;
        public string ChoiceAvailable;
        public AudioClip NoChoiceClip;
        public string NoChoice;

        private bool _cancellable;

        private void Awake()
        {
            Options.SetActive(false);
            if (Timer.GetValue(Time.time) <= 0) ShowChoice(false);
            if (!GameState.ForceChooseEndingSubscribed) SubscribeToTimerElapsedEvent();
        }

        private void SubscribeToTimerElapsedEvent()
        {
            TimerEvents.Done += () =>
            {
                Timer.Pause(Time.time);
                SceneManager.LoadScene(Scenes.Instance.Pantheon1);
            };
            GameState.ForceChooseEndingSubscribed = true;
        }

        private void ShowChoice(bool cancellable)
        {
            if (GameState.NoChoiceAvailable) SpeechBubble.Say(NoChoice, NoChoiceClip, -1);
            else SpeechBubble.Say(ChoiceAvailable, null, -1);

            if (!cancellable) Timer.Pause(Time.time);
            ParallaxController controller = GetComponent<ParallaxController>();
            controller.SetActive(false);
            controller.PlayerX = PlayerXWhileChoosing;
            controller.Player.SetActive(false);
            Options.SetActive(true);
            _cancellable = cancellable;

            if (GameState.NoChoiceAvailable) Invoke(nameof(ShowStreetEnding), GotoStreetEndingDelay);
        }

        // ReSharper disable once MemberCanBeMadeStatic.Local
        private void ShowStreetEnding() => SceneManager.LoadScene(Scenes.Instance.EndingStreet);

        private void Update()
        {
            if (_cancellable && Input.GetKeyDown(CancelWith)) CancelChoice();
        }

        private void CancelChoice()
        {
            ParallaxController controller = GetComponent<ParallaxController>();
            controller.PlayerX = PlayerX;
            controller.SetActive(true);
            Options.SetActive(false);
            controller.Player.SetActive(true);
            _cancellable = false;
            SpeechBubble.Hide();
        }
    }
}