﻿using Beyond;
using UnityEngine;

public class EndContOverlay : MonoBehaviour
{
    public float timeToEnd = 5f;
    public KeyCode keyToEnd;

    public bool OnlyShowOncePantheon1;

    public bool OnlyShowOncePantheon2;

    private bool _hidden;

    private void Awake()
    {
        if (OnlyShowOncePantheon1 && GameState.Pantheon1OverlayShown) Hide();
        if (OnlyShowOncePantheon2 && GameState.Pantheon2OverlayShown) Hide();
    }

    private void Update()
    {
        if (_hidden) return;
        if (Input.GetKey(keyToEnd)) Hide();
        if (timeToEnd <= 0.0) Hide();
        timeToEnd -= Time.deltaTime;
    }

    private void Hide()
    {
        _hidden = true;
        gameObject.SetActive(false);
        if (OnlyShowOncePantheon1) GameState.Pantheon1OverlayShown = true;
        if (OnlyShowOncePantheon2) GameState.Pantheon2OverlayShown = true;
    }
}
