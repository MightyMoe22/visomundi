using UnityEngine;

namespace Beyond {
    /// <inheritdoc />
    /// <summary>
    /// tells "Scenes" to load the backleft scene
    ///
    /// used in pantheon2 to load the HallSpace scene
    /// </summary>
    public class GoToSpaceHall : ParallaxInteractionPoint
    {
        public override KeyCode KeyCode => KeyCode.UpArrow;
        public override bool PressedThisFrame => false;
        public override Scenes.Side EntryPointFor => Scenes.Side.BackLeft;
        public override void DoInteraction(ParallaxController controller) => Scenes.Go(Scenes.Side.BackLeft);
    }
}