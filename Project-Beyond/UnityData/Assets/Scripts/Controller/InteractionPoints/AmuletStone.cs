using System;
using UnityEngine;

namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// "Insert the Amulet"-Interaction
    ///
    /// Used in left-most Pantheon Scene (Pantheon3)
    /// </summary>
    public class AmuletStone : ParallaxInteractionPoint
    {
        public enum AmuletType
        {
            Winter,
            Desert,
            Space
        }

        public GameObject UnAppliedStone, AppliedStone;
        public AmuletType Amulet;

        private void Awake()
        {
            switch (Amulet) {
                case AmuletType.Winter:
                    SetActiveExclusive(AppliedStone, UnAppliedStone, GameState.WinterAmuletApplied);
                    break;
                case AmuletType.Desert:
                    SetActiveExclusive(AppliedStone, UnAppliedStone, GameState.DesertAmuletApplied);
                    break;
                case AmuletType.Space:
                    SetActiveExclusive(AppliedStone, UnAppliedStone, GameState.SpaceAmuletApplied);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static void SetActiveExclusive(GameObject first, GameObject second, bool firstActive)
        {
            first.SetActive(firstActive);
            second.SetActive(!firstActive);
        }

        public override void DoInteraction(ParallaxController controller)
        {
            if (Amulet == AmuletType.Winter && !GameState.WinterAmulet
                || Amulet == AmuletType.Desert && !GameState.DesertAmulet
                || Amulet == AmuletType.Space && !GameState.SpaceAmulet) {
                controller.Player.SpeechBubble.Say("Hier kann ich etwas einsetzen", null);
                return;
            }

            UnAppliedStone.SetActive(false);
            AppliedStone.SetActive(true);
            AudioSource.PlayClipAtPoint(SoundEffects.Instance.PutAmulet, AppliedStone.transform.position);
            if (Amulet == AmuletType.Winter) GameState.WinterAmuletApplied = true;
            if (Amulet == AmuletType.Desert) GameState.DesertAmuletApplied = true;
            if (Amulet == AmuletType.Space) GameState.SpaceAmuletApplied = true;
        }

        protected override bool AdditionalRequirement()
        {
            switch (Amulet) {
                case AmuletType.Winter: return !GameState.WinterAmuletApplied;
                case AmuletType.Desert: return !GameState.DesertAmuletApplied;
                case AmuletType.Space: return !GameState.SpaceAmuletApplied;
                default: throw new ArgumentOutOfRangeException();
            }
        }
    }
}