using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// provides the "Direction = Left/Right/Front/Back/Idle"-Interface to Controller Classes,
    /// that starts the corresponding animation
    /// idle, left/right, front, back can all have a different animator
    /// </summary>
    public class Player : MonoBehaviour
    {
        [Header("Animations")] [SerializeField]
        private Animator IdleAnimator;

        [FormerlySerializedAs("IdleRightAnimator")] [FormerlySerializedAs("Animator")] [SerializeField]
        private Animator RightAnimator;

        [SerializeField] private GameObject FlipThis;
        [SerializeField] private Animator BackAnimator, FrontAnimator;

        [Header("Visuals")] [SerializeField] private GameObject ExclamationMark;
        public SpeechBubble SpeechBubble;

        internal bool Interaction
        {
            set
            {
                if (ExclamationMark != null) ExclamationMark.SetActive(value);
            }
        }

        public enum WalkingDirection
        {
            Idle,
            Left,
            Right,
            Back,
            Front
        }

        internal WalkingDirection Direction
        {
            set
            {
                if (_currentWalkingDirection == value) return;
                SetAnimation(value);
                _currentWalkingDirection = value;
            }
        }

        private WalkingDirection _currentWalkingDirection = WalkingDirection.Idle;

        private void SetAnimation(WalkingDirection value)
        {
            Debug.Log(value);

            IdleAnimator.gameObject.SetActive(false);
            RightAnimator.gameObject.SetActive(false);
            BackAnimator.gameObject.SetActive(false);
            FrontAnimator.gameObject.SetActive(false);

            if (value == WalkingDirection.Idle) IdleAnimator.gameObject.SetActive(true);
            if (value == WalkingDirection.Right || value == WalkingDirection.Left)
                RightAnimator.gameObject.SetActive(true);
            if (value == WalkingDirection.Back) BackAnimator.gameObject.SetActive(true);
            if (value == WalkingDirection.Front) FrontAnimator.gameObject.SetActive(true);

            switch (value) {
                case WalkingDirection.Idle:
                    IdleAnimator.Play("Idle");
                    break;
                case WalkingDirection.Left:
                    InvertXAxis(true);
                    RightAnimator.Play("Right");
                    break;
                case WalkingDirection.Right:
                    InvertXAxis(false);
                    RightAnimator.Play("Right");
                    break;
                case WalkingDirection.Back:
                    BackAnimator.Play("Back"); //TODO should be unnecessary
                    break;
                case WalkingDirection.Front:
                    FrontAnimator.Play("Front"); //TODO should be unnecessary
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(value), value, null);
            }
        }

        private void InvertXAxis(bool invert)
        {
            GameObject go = FlipThis != null
                ? FlipThis.gameObject
                : RightAnimator != null
                    ? RightAnimator.gameObject
                    : gameObject;
            float scaleY = go.transform.localScale.y;
            go.transform.localScale = new Vector3(invert ? -scaleY : scaleY, scaleY);
        }


        private void Awake()
        {
            Interaction = false;
            if (RightAnimator == null) RightAnimator = GetComponent<Animator>();
        }

        public void SetActive(bool active)
        {
            IdleAnimator.gameObject.SetActive(false);
            RightAnimator.gameObject.SetActive(false);
            BackAnimator.gameObject.SetActive(false);
            FrontAnimator.gameObject.SetActive(false);

            Interaction = false;

            if (!active) return;
            if (_currentWalkingDirection == WalkingDirection.Idle) IdleAnimator.gameObject.SetActive(true);
            if (_currentWalkingDirection == WalkingDirection.Right ||
                _currentWalkingDirection == WalkingDirection.Left) RightAnimator.gameObject.SetActive(true);
            if (_currentWalkingDirection == WalkingDirection.Back) BackAnimator.gameObject.SetActive(true);
            if (_currentWalkingDirection == WalkingDirection.Front) FrontAnimator.gameObject.SetActive(true);
        }
    }
}