namespace Beyond {
    /// <inheritdoc />
    /// <summary>
    /// Loads the "Hanoi"-minigame scene
    /// </summary>
    public class HanoiMiniGame : ParallaxInteractionPoint
    {
        public override Scenes.Side EntryPointFor => Scenes.Side.BackLeft;
        public override void DoInteraction(ParallaxController controller) => Scenes.Go(Scenes.Side.BackLeft);
    }
}