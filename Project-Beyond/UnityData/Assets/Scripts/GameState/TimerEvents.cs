using System;
using UnityEngine;

namespace Beyond
{
    /// <summary>
    /// Triggers different Events during the game
    /// </summary>
    public class TimerEvents : MonoBehaviour
    {
        public static event Action Nineteen, Half, One, Done;

        private class Stage
        {
            internal readonly Stage Next;
            public readonly int ThresholdValue;
            internal readonly Action Event;

            internal Stage(int threshold, Action action, Stage next)
            {
                ThresholdValue = threshold;
                Event = action;
                Next = next;
            }
        }

        private static readonly Stage FinalSeconds = new Stage(0, () => Done?.Invoke(), null);
        private static readonly Stage LastMinute = new Stage(60, () => One?.Invoke(), FinalSeconds);
        private static readonly Stage HalfTime = new Stage(600, () => Half?.Invoke(), LastMinute);
        private static readonly Stage FirstMinute = new Stage(1140, () => Nineteen?.Invoke(), HalfTime);

        private Stage _stage = FirstMinute;

        private void Update()
        {
            if (_stage == null || Timer.GetValue(Time.time) > _stage.ThresholdValue) return;
            _stage.Event();
            _stage = _stage.Next;
        }
    }
}