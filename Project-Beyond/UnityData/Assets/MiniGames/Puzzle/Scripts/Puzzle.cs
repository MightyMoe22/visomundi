﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Beyond;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.Serialization;

public class Puzzle : MonoBehaviour
{

    [FormerlySerializedAs("image")] public Texture2D Image;
    [FormerlySerializedAs("blocksPerLine")] public int BlocksPerLine = 4;
    [FormerlySerializedAs("shuffleLength")] public int ShuffleLength = 20;
    [FormerlySerializedAs("defaultMoveDuration")] public float DefaultMoveDuration = .2f;
    [FormerlySerializedAs("shuffleMoveDuration")] public float ShuffleMoveDuration = .1f;

    private enum PuzzleState { Solved, Shuffling, InPlay };

    private PuzzleState _state;

    private Block _emptyBlock;
    private Block[,] _blocks;
    private Queue<Block> _inputs;
    private bool _blockIsMoving;
    private int _shuffleMovesRemaining;
    private Vector2Int _prevShuffleOffset;
    private AudioManager audioManager;

    private PuzzleState State
    {
        set
        {
            if (value == PuzzleState.Solved) {
                GameState.DesertQuizAllowed = true;
                GameState.NotifyInventory();
                Scenes.Go(Scenes.Side.Front);
            }

            _state = value;
        }
    }

    private void Start()
    {
        audioManager = FindObjectOfType<AudioManager>();
        CreatePuzzle();

    }

    private void Update()
    {
        if (_state == PuzzleState.Solved && Input.GetKeyDown(KeyCode.Space)) StartShuffle();
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.DownArrow)) Scenes.Go(Scenes.Side.Front);
    }

    private void CreatePuzzle()
    {
        _blocks = new Block[BlocksPerLine, BlocksPerLine];
        Texture2D[,] imageSlices = ImageSlicer.GetSlices(Image, BlocksPerLine);
        for (int y = 0; y < BlocksPerLine; y++)
        {
            for (int x = 0; x < BlocksPerLine; x++)
            {
                GameObject blockObj = GameObject.CreatePrimitive(PrimitiveType.Quad);
                blockObj.transform.parent = transform;
                blockObj.transform.localScale = Vector3.one;
                blockObj.transform.localPosition = (BlocksPerLine - 1) * .5f * -Vector2.one + new Vector2(x, y);

                Block block = blockObj.AddComponent<Block>();
                block.OnBlockPressed += PlayerMoveBlockInput;
                block.OnFinishedMoving += OnBlockFinishedMoving;
                block.Init(new Vector2Int(x, y), imageSlices[x, y]);
                _blocks[x, y] = block;

                if (y == 0 && x == BlocksPerLine - 1)
                {

                    _emptyBlock = block;
                }
            }

        }

        Camera.main.orthographicSize = BlocksPerLine * .55f;
        _inputs = new Queue<Block>();
    }

    private void PlayerMoveBlockInput(Block blockToMove)
    {
        if (_state == PuzzleState.InPlay)
        {
            _inputs.Enqueue(blockToMove);
            MakeNextPlayerMove();
        }
    }

    private void MakeNextPlayerMove()
    {
        while (_inputs.Count > 0 && !_blockIsMoving)
        {
            MoveBlock(_inputs.Dequeue(), DefaultMoveDuration);
        }
    }

    private void MoveBlock(Block blockToMove, float duration)
    {
        if ((blockToMove.coord - _emptyBlock.coord).sqrMagnitude == 1)
        {
            _blocks[blockToMove.coord.x, blockToMove.coord.y] = _emptyBlock;
            _blocks[_emptyBlock.coord.x, _emptyBlock.coord.y] = blockToMove;

            Vector2Int targetCoord = _emptyBlock.coord;
            _emptyBlock.coord = blockToMove.coord;
            blockToMove.coord = targetCoord;
            Vector2 targetPosition = _emptyBlock.transform.position;
            _emptyBlock.transform.position = blockToMove.transform.position;
            blockToMove.MoveToPosition(targetPosition, duration);
            _blockIsMoving = true;
        }
    }

    private void OnBlockFinishedMoving()
    {
        _blockIsMoving = false;
        CheckIfSolved();

        if (_state == PuzzleState.InPlay)
        {
            MakeNextPlayerMove();
        }
        else if (_state == PuzzleState.Shuffling)
        {
            if (_shuffleMovesRemaining > 0)
            {
                MakeNextShuffleMove();
            }
            else
            {
                State = PuzzleState.InPlay;
            }
        }
    }

    private void StartShuffle()
    {
        State = PuzzleState.Shuffling;
        _shuffleMovesRemaining = ShuffleLength;
        _emptyBlock.gameObject.SetActive(false);
        MakeNextShuffleMove();
    }

    private void MakeNextShuffleMove()
    {
        Vector2Int[] offsets = { new Vector2Int(1, 0), new Vector2Int(-1, 0), new Vector2Int(0, 1), new Vector2Int(0, -1) };
        int randomIndex = Random.Range(0, offsets.Length);

        for (int i = 0; i < offsets.Length; i++)
        {
            Vector2Int offset = offsets[(randomIndex + i) % offsets.Length];
            if (offset == _prevShuffleOffset * -1) continue;
            
            Vector2Int moveBlockCoord = _emptyBlock.coord + offset;
            if (moveBlockCoord.x < 0 || moveBlockCoord.x >= BlocksPerLine || moveBlockCoord.y < 0 ||
                moveBlockCoord.y >= BlocksPerLine) continue;
            
            MoveBlock(_blocks[moveBlockCoord.x, moveBlockCoord.y], ShuffleMoveDuration);
            _shuffleMovesRemaining--;
            _prevShuffleOffset = offset;
            break;
        }
    }

    private void CheckIfSolved()
    {
        if (_blocks.Cast<Block>().Any(block => !block.IsAtStartingCoord())) return;
        State = PuzzleState.Solved;
        _emptyBlock.gameObject.SetActive(true);
    }
}


