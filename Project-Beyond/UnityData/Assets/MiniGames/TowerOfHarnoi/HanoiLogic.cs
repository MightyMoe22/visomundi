﻿using System.Collections.Generic;
using UnityEngine;

namespace Beyond
{
    /// <summary>
    /// Contains the logic how the minigame is setup and defines when it is finished
    /// </summary>
    public class HanoiLogic : MonoBehaviour
    {
        public List<Tower> towers = new List<Tower>();
        public List<Brick> bricks = new List<Brick>();

        public bool MouseEmpty = true;


        private void Start()
        {
            for (int i = 0; i < bricks.Capacity; i++) {
                bricks[i].BrickSetUp(this, i + 1);
            }

            foreach (Tower tower in towers) {
                tower.SetHanoiLogic(this);
            }

            PutBricksToStart();
        }

        private void PutBricksToStart()
        {
            Tower start = towers[0];
            for (int i = bricks.Capacity - 1; i >= 0; i--) {
                start.AddBrick(bricks[i]);
                bricks[i].SetStartTower(start);
            }
        }

        private void Update()
        {
            if (towers[2].bricks.Count == 3) {
                //Debug.Log("You won!!!");
                GameState.WinterQuizAllowed = true;
                GameState.NotifyInventory();
                Scenes.Go(Scenes.Side.Front);
            }

            if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.DownArrow)) Scenes.Go(Scenes.Side.Front);
        }
    }
}