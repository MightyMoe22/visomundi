﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Beyond
{
    /// <summary>
    /// Contains the logic how a brick is setup and how it can be used by mouse
    /// </summary>
    public class Brick : MonoBehaviour
    {
        private int brickSize;
        private HanoiLogic hanoiLogic = null;
        private Tower nextTower = null;
        private Tower currentTower = null;
        private bool grabbed = false;
        private bool canBeDrop = false;

        public int GetBrickSize() => brickSize;

        public void BrickSetUp(HanoiLogic hanoiLogic, int size)
        {
            this.hanoiLogic = hanoiLogic;
            brickSize = size;
            Debug.Log("SetupBrick");
        }

        public void SetStartTower(Tower tower)
        {
            currentTower = tower;
        }

        private void Update()
        {
            if (grabbed)
                FollowMouse();
        }

        private void OnMouseDown()
        {
            if (hanoiLogic.MouseEmpty && currentTower.RemoveBrick(this)) {
                hanoiLogic.MouseEmpty = false;
                grabbed = true;
            }
        }

        private void OnMouseUp()
        {
            if (canBeDrop) {
                if (!nextTower.AddBrick(this))
                    currentTower.AddBrick(this);
                else {
                    currentTower = nextTower;
                    nextTower = null;
                }
            } else {
                currentTower.AddBrick(this);
            }

            canBeDrop = false;
            grabbed = false;
            hanoiLogic.MouseEmpty = true;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.tag.Equals("Tower")) {
                Debug.Log("Enter Tower: " + collision.name);
                nextTower = collision.gameObject.GetComponent<Tower>();
                canBeDrop = true;
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.gameObject.tag.Equals("Tower")) {
                Debug.Log("Exit Tower: " + collision.name);
                canBeDrop = false;
                nextTower = null;
            }
        }

        private void FollowMouse()
        {
            Vector3 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(worldPoint.x, worldPoint.y, 0);
        }
    }
}
