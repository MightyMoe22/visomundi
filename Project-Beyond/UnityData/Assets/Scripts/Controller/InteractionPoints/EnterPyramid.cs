namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// Only allows pyramid entry, when the pyramid was already opened
    /// </summary>
    public class EnterPyramid : GoToBackScene
    {
        protected override bool AdditionalRequirement() => GameState.PyramidOpened;
    }
}