using UnityEngine;
using UnityEngine.UI;

namespace Beyond
{
    /// <summary>
    /// Shows the 4 quiz questions and answers on 8 pages and moves the hand sprite to the mouse position
    /// </summary>
    public class QuizController : MonoBehaviour
    {
        [Header("Colors")] public Color HighlightColor = Color.white;
        public Color CorrectColor = Color.green;
        public Color IncorrectColor = Color.red;

        [Header("Questions")] public QuizTexts.Types QuestionSet;

        [Header("References")] public GameObject Hand;
        public Text QuestionText;
        public QuizOptions OptionsPage;
        public Text AnswerText;
        public QuizButton NextPageButton;

        private QuizTexts.QuestionSet[] _questions;
        private int _pageNumber = -1;
        private int _correctChoice;
        private bool _chosen;
        private bool _allCorrect = true;

        private void Awake()
        {
            _questions = QuizTexts.GetQuestions(QuestionSet);
            Cursor.visible = false;
            OptionsPage.HighlightColor = NextPageButton.HighlightColor = HighlightColor;
            OptionsPage.OptionChosen += CheckChoice;
            NextPageButton.ButtonPressed += ShowNextPage;
            ShowNextPage();
        }

        private void CheckChoice(int choice)
        {
            if (_chosen) return;
            _chosen = true;
            
            OptionsPage.SetColor(choice, choice == _correctChoice ? CorrectColor : IncorrectColor);
            OptionsPage.SetColor(_correctChoice, CorrectColor);
            
            _allCorrect &= choice == _correctChoice;
            if (_allCorrect & _pageNumber == _questions.Length * 2 - 2) {
                AudioSource.PlayClipAtPoint(SoundEffects.Instance.PickupItem, Vector3.zero);
                QuizTexts.SetQuizComplete(QuestionSet);
            }

            NextPageButton.gameObject.SetActive(true);
        }

        private void ShowNextPage()
        {
            _pageNumber++;
            _chosen = false;
            if (_pageNumber >= _questions.Length * 2) Scenes.Go(Scenes.Side.Front);
            else {
                QuizTexts.QuestionSet question = _questions[_pageNumber / 2];
                if (_pageNumber % 2 == 0) {
                    AnswerText.gameObject.SetActive(false);
                    OptionsPage.gameObject.SetActive(true);
                    
                    OptionsPage.ResetColor();
                    QuestionText.text = question.Question;
                    OptionsPage.SetText(question);
                    _correctChoice = question.CorrectChoice;
                    
                    NextPageButton.gameObject.SetActive(false);
                } else {
                    AnswerText.gameObject.SetActive(true);
                    OptionsPage.gameObject.SetActive(false);
                    AnswerText.text = question.Answer;
                }
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.DownArrow)) Scenes.Go(Scenes.Side.Front);

            // ReSharper disable once PossibleNullReferenceException
            Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            position.z = 0;
            Hand.transform.position = position;
        }
    }
}