using System.Collections;
using UnityEngine;

namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// Triggers the "swoooosh" animation over the pit in winter2 scene, also see JumpRightOverPit
    /// </summary>
    public class JumpLeftOverPit : ParallaxInteractionPoint
    {
        public JumpRightOverPit LeftSide;
        public GameObject SpecialSprite;

        public override KeyCode KeyCode => KeyCode.LeftArrow;
        public override bool PressedThisFrame => false;

        private void Awake()
        {
            SpecialSprite.SetActive(false);
        }

        // ReSharper disable once RedundantAssignment
        public override void DoInteraction(ParallaxController controller)
        {
            StartCoroutine(Flying(controller));
        }

        private IEnumerator Flying(ParallaxController controller)
        {
            controller.SetActive(false);
            controller.Player.SetActive(false);
            SpecialSprite.SetActive(true);
            controller.Player.transform.localScale = Vector3.one - 2 * Vector3.right;

            AudioSource.PlayClipAtPoint(SoundEffects.Instance.Swoosh, SpecialSprite.transform.position);
            float speedPerSecond = (LeftSide.PlayerX - PlayerX) / SoundEffects.Instance.Swoosh.length;
            while (controller.PlayerX > LeftSide.PlayerX) {
                controller.PlayerX += Time.deltaTime * speedPerSecond;
                yield return null;
            }

            controller.PlayerX = LeftSide.PlayerX;

            controller.Player.transform.localScale = Vector3.one;
            SpecialSprite.SetActive(false);
            controller.Player.SetActive(true);
            controller.SetActive(true);
        }
    }
}