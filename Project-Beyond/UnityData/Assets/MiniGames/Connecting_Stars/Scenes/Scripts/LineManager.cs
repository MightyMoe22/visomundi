﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// LineManager hat die Aufgabe die Linien zwischen den Sternen zu zeichnen
public class LineManager : MonoBehaviour
{
    
    public Color lineColor;
    public Material lineMaterial;
    public Color completeColor;
    public Color falseColor;
   

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    //Linie zwischen zwei Positionen zeichnen
    public void DrawLine(Vector3 start, Vector3 end, float duration = 0.2f)
    {
        GameObject lineObject = new GameObject();
        lineObject.transform.parent = transform;

   

        LineRenderer line = lineObject.AddComponent(typeof(LineRenderer)) as LineRenderer;
        line.startColor = lineColor;
        line.endColor = lineColor;
        line.material = lineMaterial;
        line.widthCurve = AnimationCurve.Linear(0f, 0.05f, 1f, 0.05f);
       
       
        line.SetPosition(0, start);
        line.SetPosition(1, end);
    
    }


    //Alle Linien werden zum Beispiel am Levelende zerstört
    public void LineReset()
    {
        foreach(Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }


    //Linien werden gefärbt je nach Erfolg des Levels
    public void LevelComplete(bool successfull)
    {
        Color c = successfull ? completeColor : falseColor; 
        foreach (Transform child in transform)
        {
            child.gameObject.GetComponent<LineRenderer>().startColor = c;
            child.gameObject.GetComponent<LineRenderer>().endColor = c;
        }
    }
      
}
