# Visomundi

2089: Ab dem 18. Lebensjahr werden die Menschen von der Regierung 
gezwungen, sich einer von drei Weltanschauungen anzuschließen. 
Die sogenannte Weltentaufe.

Erkunde drei Themenbereiche, in welchen du dich für eine Gruppe entscheiden musst.
Setze dich mit den Welten auseinander und hinterfrage die Weltanschauungen. 
Gibt es einen Weg, der Entscheidung zu entkommen?


## Trailer 
https://vimeo.com/349819626

## Full Playthrough
![](Project-Beyond/trailerOhneRobotstimmen.mp4)


# Eine interaktive Geschichte von

## Anna Sedakina
- Artstyle Space/Desert/Winterworld 
- Intro/Outrovideo
- Lockpick Assets
- Interface vom Quiz
- Charakter Winter und Space
- Tonnenende

## Stefan Brauner
- Artstyle Pantheon
- Outro Animation (Ending 1–3)
- Tower of Hanoi Assets
- Charakterdesign & Animation
- Sounddesign & Musik
- Title Design
- Trailer
- Poster

## Maurice Langer
- Asset Development
- Minigame: Lockpick Tower of Hanoi
- Parallax-Tech Design (keine Implementierung)
- VFX

## Julian Müller
- Implementierung Parallax Effekt, Interaktionen, Quiz, Spiellogik, UI
- Setup für Bergwerk

## John Nitsche
- Minigame: Sterne verbinden
- Minigame: Puzzle Game
- Rigging und Animation (Winter, Space)

## Betreut durch
- Prof. Henning Rogge-Pott
- Prof. Dr. Sebastian von Mammen
- Prof. Dr. Nicholas Müller
