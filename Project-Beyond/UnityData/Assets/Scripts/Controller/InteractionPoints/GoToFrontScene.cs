using UnityEngine;

namespace Beyond {
    /// <inheritdoc />
    /// <summary>
    /// Tells "Scenes" to load the scene in front of the current scene
    /// </summary>
    public class GoToFrontScene : ParallaxInteractionPoint
    {
        public override KeyCode KeyCode => KeyCode.DownArrow;
        public override bool PressedThisFrame => true;
        public override Scenes.Side EntryPointFor => Scenes.Side.Front;
        public override void DoInteraction(ParallaxController controller) => Scenes.Go(Scenes.Side.Front);
    }
}