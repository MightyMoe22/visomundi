using UnityEngine;

namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// Tells "Scenes" to load the scene left of the current scene
    /// </summary>
    public class GoToLeftScene : ParallaxInteractionPoint
    {
        public override KeyCode KeyCode => KeyCode.LeftArrow;
        public override bool PressedThisFrame => false;
        public override Scenes.Side EntryPointFor => Scenes.Side.Left;
        public override void DoInteraction(ParallaxController controller) => Scenes.Go(Scenes.Side.Left);
    }
}