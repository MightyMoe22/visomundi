using UnityEngine;

namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// Allows melting the ice block, if player has already picked up the bunsenbrenner in moonbase scene
    /// </summary>
    public class MeltIceBlock : ParallaxInteractionPoint
    {
        public GameObject BoulderIce, BoulderMolten;
        public string NoBunsenbrennerText;
        public AudioClip BunsenbrennerSound;
        public AudioSource AudioSource;
        private ParallaxController _parallaxController;

        private void Awake()
        {
            BoulderIce.SetActive(!GameState.StoneOpened);
            BoulderMolten.SetActive(GameState.StoneOpened);
        }

        public override void DoInteraction(ParallaxController controller)
        {
            _parallaxController = controller;
            if (GameState.BunsenBrenner) {
                AudioSource.clip = BunsenbrennerSound;
                AudioSource.Play();
                _parallaxController.SetActive(false);
                Invoke(nameof(ApplyMelting), BunsenbrennerSound.length);
            } else {
                _parallaxController.Player.SpeechBubble.Say(NoBunsenbrennerText, null);
            }
        }

        private void ApplyMelting()
        {
            BoulderIce.SetActive(false);
            BoulderMolten.SetActive(true);
            GameState.StoneOpened = true;
            GameState.NotifyInventory();
            _parallaxController.SetActive(true);
        }

        protected override bool AdditionalRequirement() => !GameState.StoneOpened;
    }
}