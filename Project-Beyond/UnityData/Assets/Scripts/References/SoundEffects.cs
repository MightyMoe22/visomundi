using UnityEngine;

namespace Beyond
{
    /// <summary>
    /// Singleton, that holds different sound effect clips
    /// </summary>
    [CreateAssetMenu(fileName = "SoundEffects")]
    public class SoundEffects : ScriptableObject
    {
        private static SoundEffects _instance;
        internal static SoundEffects Instance =>
            _instance != null ? _instance : _instance = Resources.Load<SoundEffects>("SoundEffects");

        public static AudioClip MoveSound => Random.Range(0, 2) == 0 ? Instance.MoveSound1 : Instance.MoveSound2;
        
        public AudioClip QuizButton;
        public AudioClip PickupItem;
        public AudioClip PutAmulet;

        public AudioClip MoveSound1, MoveSound2;

        public AudioClip Swoosh;
    }
}