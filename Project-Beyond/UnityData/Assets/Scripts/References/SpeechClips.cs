using UnityEngine;

namespace Beyond
{
    /// <summary>
    /// Singleton, that holds different audioclips played at different points in time
    /// </summary>
    [CreateAssetMenu]
    public class SpeechClips : ScriptableObject
    {
        private static SpeechClips _instance;

        public static SpeechClips Instance =>
            _instance != null ? _instance : _instance = Resources.Load<SpeechClips>("SpeechClips");

        [Header("Time Warnings")] public AudioClip WhyAreYouWaiting;
        public AudioClip HalfTime;
        public AudioClip OneMinute;
        public AudioClip TimeElapsed;

        [Header("AreaWelcomes")] public AudioClip Space;
        public AudioClip Winter;
        public AudioClip Desert;
    }
}