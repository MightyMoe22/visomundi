using UnityEngine;

namespace Beyond
{
    /// <summary>
    /// Shows the correct choice sprites during the enddecision
    /// </summary>
    public class ShowAvailableOptions : MonoBehaviour
    {
        public SpriteRenderer Cube, NotCube, Flat, NotFlat, Round, NotRound;

        private void Awake()
        {
            SetActiveExclusive(Cube, NotCube, GameState.DesertQuizSucceeded);
            SetActiveExclusive(Flat, NotFlat, GameState.WinterQuizSucceeded);
            SetActiveExclusive(Round, NotRound, GameState.SpaceQuizSucceeded);
        }

        private static void SetActiveExclusive(Component first, Component second, bool firstActive)
        {
            first.gameObject.SetActive(firstActive);
            second.gameObject.SetActive(!firstActive);
        }
    }
}