using System;
using UnityEngine;

namespace Beyond
{
    /// <summary>
    /// Singleton that holds all quiz texts
    /// </summary>
    [CreateAssetMenu]
    public class QuizTexts : ScriptableObject
    {
        public enum Types
        {
            Desert, Space, Winter
        }
        
        [Serializable]
        public class QuestionSet
        {
            [Multiline(2)] public string Question;
            [Multiline(3)] public string Option1, Option2, Option3, Option4;
            [Multiline(5)] public string Answer;
            [Range(1, 4)] public int CorrectChoice;
        }

        private static QuizTexts _instance;
        private static QuizTexts Instance =>
            _instance != null ? _instance : _instance = Resources.Load<QuizTexts>("QuizTexts");

        public QuestionSet[] DesertQuestions, SpaceQuestions, WinterQuestions;

        public static QuestionSet[] GetQuestions(Types type)
        {
            switch (type) {
                case Types.Desert:
                    return Instance.DesertQuestions;
                case Types.Space:
                    return Instance.SpaceQuestions;
                case Types.Winter:
                    return Instance.WinterQuestions;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        public static void SetQuizComplete(Types type)
        {
            switch (type) {
                case Types.Desert:
                    GameState.DesertQuizSucceeded = true;
                    break;
                case Types.Space:
                    GameState.SpaceQuizSucceeded = true;
                    break;
                case Types.Winter:
                    GameState.WinterQuizSucceeded = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
            GameState.NotifyInventory();
        }

        public static bool IsQuizAllowed(Types type)
        {
            switch (type) {
                case Types.Desert:
                    return GameState.DesertQuizAllowed;
                case Types.Space:
                    return GameState.SpaceQuizAllowed;
                case Types.Winter:
                    return GameState.WinterQuizAllowed;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }
    }
}