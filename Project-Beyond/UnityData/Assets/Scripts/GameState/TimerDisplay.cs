using UnityEngine;
using UnityEngine.UI;

namespace Beyond
{
    [RequireComponent(typeof(Text))]
    public class TimerDisplay : MonoBehaviour
    {
        private Text _text;

        private void Awake()
        {
            _text = GetComponent<Text>();
        }

        private void Update()
        {
            int timerValue = (int) Timer.GetValue(Time.time);
            _text.text = $"{timerValue / 60:00} : {timerValue % 60:00}";
        }
    }
}