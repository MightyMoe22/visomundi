using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// Responsible for Background music and announcements (time / world information)
    /// </summary>
    [RequireComponent(typeof(Canvas))]
    public class SoundAndTimerSetup : MonoBehaviour
    {
        public SoundScapes.Setting SoundScape;
        public bool PlayWelcomeClip;

        public bool FadeIn;
        public SpriteRenderer WhiteScreen;
        
        private void Awake()
        {
            if (!FadeIn) WhiteScreen.gameObject.SetActive(false);
            if (FadeIn && Scenes.EntrySide == Scenes.Side.Front) StartCoroutine(nameof(DoFadeIn));
            if (!_setup) Setup();
            BackgroundSetting = SoundScape;
            GetComponent<Canvas>().worldCamera = Camera.main;
            if (PlayWelcomeClip) PlayWelcomeCLip();
        }

        private IEnumerator DoFadeIn()
        {
            WhiteScreen.gameObject.SetActive(true);
            WhiteScreen.color = Color.white;
            for (float alpha = 1; alpha > 0; alpha -= 0.01f) {
                WhiteScreen.color = new Color(1, 1, 1, alpha);
                yield return null;
            }
            WhiteScreen.color = Color.clear;
            WhiteScreen.gameObject.SetActive(false);
        }

        private void PlayWelcomeCLip()
        {
            switch (SoundScape) {
                case SoundScapes.Setting.Halls:
                    //if (!_playedWelcome) PlaySpeechClip(SpeechClips.Instance.Welcome);
                    //_playedWelcome = true;
                    break;
                case SoundScapes.Setting.Desert:
                    if (!GameState.PlayedWelcomeDesert) PlaySpeechClip(SpeechClips.Instance.Desert);
                    GameState.PlayedWelcomeDesert = true;
                    break;
                case SoundScapes.Setting.Space:
                    if (!GameState.PlayedWelcomeSpace) PlaySpeechClip(SpeechClips.Instance.Space);
                    GameState.PlayedWelcomeSpace = true;
                    break;
                case SoundScapes.Setting.Winter:
                    if (!GameState.PlayedWelcomeWinter) PlaySpeechClip(SpeechClips.Instance.Winter);
                    GameState.PlayedWelcomeWinter = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }


        #region static Properties and Setup Methods

        private static SoundScapes.Setting BackgroundSetting
        {
            set
            {
                if (!_setup) Setup();
                SoundScapes.GetSnapshot(value).TransitionTo(SoundScapes.Instance.TransitionTime);
            }
        }

        private static bool _setup;
        private static AudioSource _speechSource;
        private static GameObject _timerEvents;

        private static void Setup()
        {
            if (_setup) return;
            SetupAudioSources();
            RegisterTimerEvents();
            _setup = true;
        }

        private static void RegisterTimerEvents()
        {
            _timerEvents = new GameObject("TimerEvents");
            _timerEvents.AddComponent<TimerEvents>();
            DontDestroyOnLoad(_timerEvents);

            TimerEvents.Nineteen += WhyAreYouWaitingIfInMainScene;
            TimerEvents.Half += () => PlaySpeechClip(SpeechClips.Instance.HalfTime);
            TimerEvents.One += () => PlaySpeechClip(SpeechClips.Instance.OneMinute);
            TimerEvents.Done += () => PlaySpeechClip(SpeechClips.Instance.TimeElapsed);
        }

        private static void WhyAreYouWaitingIfInMainScene()
        {
            if (SceneManager.GetActiveScene().name == Scenes.Instance.Pantheon1) 
                PlaySpeechClip(SpeechClips.Instance.WhyAreYouWaiting);
        }

        internal static void PlaySpeechClip(AudioClip clip)
        {
            _speechSource.Stop();
            _speechSource.clip = clip;
            _speechSource.Play();
        }

        private static void SetupAudioSources()
        {
            GameObject sound = new GameObject("Sound");
            DontDestroyOnLoad(sound);

            void AddSourceToMixer(AudioClip clip, AudioMixerGroup mixerGroup)
            {
                AudioSource halls = sound.AddComponent<AudioSource>();
                halls.clip = clip;
                halls.outputAudioMixerGroup = mixerGroup;
                halls.loop = true;
                halls.Play();
            }

            AddSourceToMixer(SoundScapes.Instance.HallsClip, SoundScapes.Instance.HallsGroup);
            AddSourceToMixer(SoundScapes.Instance.DesertClip, SoundScapes.Instance.DesertGroup);
            AddSourceToMixer(SoundScapes.Instance.SpaceClip, SoundScapes.Instance.SpaceGroup);
            AddSourceToMixer(SoundScapes.Instance.WinterClip, SoundScapes.Instance.WinterGroup);

            _speechSource = sound.AddComponent<AudioSource>();
            _speechSource.loop = false;
        }

        #endregion

        public static void Clear()
        {
            if (!_setup) return;
            _setup = false;
            Destroy(_speechSource.gameObject);
            Destroy(_timerEvents);
        }
    }
}