﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// AudioManager steuert nur Sound
public class AudioManager : MonoBehaviour
{
    public AudioClip Intro, SternbildFalsch, SternbildRichtig;
    public float volumeScale;
    public AudioClip[] sternSounds;

    private AudioSource audiosource;
    
    
    
    // Start is called before the first frame update
    void Start()
    {
        audiosource = GetComponent<AudioSource>();
        audiosource.clip = Intro;
        audiosource.Play();
        audiosource.loop = true;



    }

    

    public void SternbildFalschSound()
    {
        
        audiosource.PlayOneShot(SternbildFalsch, volumeScale);
    }

    public void SternbildRichtigSound()
    {
        audiosource.PlayOneShot(SternbildRichtig, volumeScale);
    }

    public void SternSound(int index)
    {
        index = index - 1;
        index = index > 8 ? 8 : index;
        audiosource.PlayOneShot(sternSounds[index], volumeScale);
    }
}
