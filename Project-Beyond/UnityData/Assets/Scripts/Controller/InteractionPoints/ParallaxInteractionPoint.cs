using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// This abstract base class defines an interaction point for a scene that uses a "Parallax Controller"
    /// 
    /// The Parallax Controller is responsible for triggering the "DoInteraction" Method,
    /// when "InteractionPossible" return true (the Player is at x=PlayerX(+/-XDiff) and y=PlayerLayer)
    /// and the Key KeyCode is pressed.
    /// </summary>
    [RequireComponent(typeof(ParallaxController))]
    public abstract class ParallaxInteractionPoint : MonoBehaviour
    {
        [Range(-1f, 1f)] public float PlayerX;
        [Range(0, 1f)] public float XDiff = 0.03f;
        public int PlayerLayer;

        public virtual KeyCode KeyCode { get; } = KeyCode.Space;
        public virtual bool PressedThisFrame { get; } = true;
        public virtual Scenes.Side EntryPointFor { get; } = Scenes.Side.None;

        public virtual bool Possible { protected get; set; }

        public abstract void DoInteraction(ParallaxController controller);

        public bool InteractionPossible(int playerLayer, float playerX) =>
            PlayerLayer == playerLayer && Math.Abs(PlayerX - playerX) < XDiff && AdditionalRequirement();

        protected virtual bool AdditionalRequirement() => true;
    }
}