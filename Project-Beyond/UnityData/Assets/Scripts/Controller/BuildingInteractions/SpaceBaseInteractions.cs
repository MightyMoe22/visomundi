using System;
using UnityEngine;

namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// "Picking up the Bunsenbrenner"-Interaction
    /// "GoTo Lockpicking Minigame"-Interaction
    /// "Picking up the Bunsenbrenner"-Interaction
    ///
    /// used in the MoonBase scene
    /// </summary>
    [RequireComponent(typeof(BuildingController))]
    [ExecuteAlways]
    public class SpaceBaseInteractions : MonoBehaviour
    {
        [Range(0, 1)] public float PlayerXRange = 0.03f;
        [Range(-1, 1)] public float Bunsenbrenner;
        [Range(-1, 1)] public float Lock;

        public GameObject BunsenSprite;
        public GameObject OpenSprite;
        public GameObject AmuletSprite;
        [Multiline(2)] public string FehlText, PickupText;

        private BuildingController _buildingController;

        private void Awake()
        {
            _buildingController = GetComponent<BuildingController>();
            BunsenSprite.SetActive(!GameState.BunsenBrenner);
            OpenSprite.SetActive(GameState.SpaceAmuletAvailable);
            AmuletSprite.SetActive(!GameState.SpaceAmulet);
        }

        private void Update()
        {
            if (Math.Abs(_buildingController.PlayerX - Bunsenbrenner) < PlayerXRange && !GameState.BunsenBrenner) {
                _buildingController.Player.Interaction = true;
                if (!Input.GetKeyDown(KeyCode.Space)) return;
                DoBunsenInteraction();
            } else if (Math.Abs(_buildingController.PlayerX - Lock) < PlayerXRange) {
                if (!GameState.SpaceAmuletAvailable) {
                    _buildingController.Player.Interaction = true;
                    if (!Input.GetKeyDown(KeyCode.Space)) return;
                    DoLockInteraction();
                } else if (!GameState.SpaceAmulet) {
                    _buildingController.Player.Interaction = true;
                    if (!Input.GetKeyDown(KeyCode.Space)) return;
                    DoAmuletInteraction();
                }
            } else _buildingController.Player.Interaction = false;
        }

        private void DoAmuletInteraction()
        {
            AmuletSprite.SetActive(false);
            GameState.SpaceAmulet = true;
            GameState.NotifyInventory();
            AudioSource.PlayClipAtPoint(SoundEffects.Instance.PickupItem, new Vector3(Lock, 0));
            _buildingController.Player.SpeechBubble.Say(Amulets.GetNextText(), Amulets.GetClip());
        }

        private void DoLockInteraction()
        {
            if (GameState.Chisel) Scenes.Go(Scenes.Side.Back);
            else _buildingController.Player.SpeechBubble.Say(FehlText, null);
        }

        private void DoBunsenInteraction()
        {
            BunsenSprite.SetActive(false);
            GameState.BunsenBrenner = true;
            GameState.NotifyInventory();
            _buildingController.Player.SpeechBubble.Say(PickupText, null);
            AudioSource.PlayClipAtPoint(SoundEffects.Instance.PickupItem, BunsenSprite.transform.position);
        }
    }
}