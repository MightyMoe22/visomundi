using UnityEngine;

namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// Tells "Scenes" to loads the "backright" when up arrow is pressed
    /// 
    /// used in pantheon2 to reach HallDesert
    /// </summary>
    public class GoToDesertHall : ParallaxInteractionPoint
    {
        public override KeyCode KeyCode => KeyCode.UpArrow;
        public override bool PressedThisFrame => false;
        public override Scenes.Side EntryPointFor => Scenes.Side.BackRight;
        public override void DoInteraction(ParallaxController controller) => Scenes.Go(Scenes.Side.BackRight);
    }
}