using UnityEngine;

namespace Beyond
{
    /// <summary>
    /// Plays the Welcome Message when the Player reaches the overseer for the first time.
    /// 
    /// After this welcome speech the Timer is started.
    /// </summary>
    [RequireComponent(typeof(ParallaxController))]
    public class WelcomeMessage : MonoBehaviour
    {
        [Range(-1, 1)] public float PlayerX;
        public AudioClip WelcomeClip;

        public Animator Aufseher;
        public string AnimationName;
        
        private ParallaxController _controller;

        private void Awake()
        {
            _controller = GetComponent<ParallaxController>();
        }

        private void Update()
        {
            if (GameState.WelcomeClipPlayed || _controller.PlayerX > PlayerX) return;
            AudioSource.PlayClipAtPoint(WelcomeClip, Aufseher.transform.position);
            _controller.SetActive(false);
            Invoke(nameof(ReactivateController), WelcomeClip.length);
            GameState.WelcomeClipPlayed = true;
        }

        private void ReactivateController()
        {
            Aufseher.Play(AnimationName);
            Timer.Resume(Time.time);
            _controller.SetActive(true);
        }
    }
}