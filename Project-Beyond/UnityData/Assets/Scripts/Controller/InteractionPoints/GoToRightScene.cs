using UnityEngine;

namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// tells "Scenes" to load the scene right of the current scene
    /// </summary>
    public class GoToRightScene : ParallaxInteractionPoint
    {
        public override KeyCode KeyCode => KeyCode.RightArrow;
        public override bool PressedThisFrame => false;
        public override Scenes.Side EntryPointFor => Scenes.Side.Right;
        public override void DoInteraction(ParallaxController controller) => Scenes.Go(Scenes.Side.Right);
    }
}