using UnityEngine;

namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// Blocks the Up-Arrow Key by doing nothing
    ///
    /// Used in Desert1, Space1, Winter1
    /// </summary>
    public class BlockBackMovement : ParallaxInteractionPoint
    {
        public override KeyCode KeyCode => KeyCode.UpArrow;
        public override bool PressedThisFrame => false;
        public override void DoInteraction(ParallaxController controller) {}
    }
}