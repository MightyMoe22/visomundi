using UnityEngine;

namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// Tells the "ParallaxController" of this scene, that the Player should be placed at this start position
    /// when not entering from another scene
    ///
    /// Used in the first scene (pantheon1)
    /// </summary>
    [RequireComponent(typeof(ParallaxController))]
    public class GameStartPosition : MonoBehaviour
    {
        [Range(-1, 1)] public float PlayerX;
        public int PlayerLayer;
        public Player.WalkingDirection WalkingDirection = Player.WalkingDirection.Left;
    }
}