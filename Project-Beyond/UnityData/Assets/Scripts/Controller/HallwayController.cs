using System;
using UnityEngine;

namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// Allows Player to walk front/back in Hall scenes
    /// </summary>
    [ExecuteInEditMode]
    public sealed class HallwayController : MonoBehaviour
    {
        public Player Player;

        [Range(-1f, 1f)] public float Destination = 0.7f;
        public float EntryScale = 1;
        public float DestinationScale = 0.2f;

        public float PlayerSpeed = 0.4f;
        [Range(0f, 1f)] public float PlayerDepth;

        private float _halfWidth;

        private void Awake()
        {
            _halfWidth = Camera.main.orthographicSize * Camera.main.aspect;
            switch (Scenes.EntrySide) {
                case Scenes.Side.Back:
                case Scenes.Side.BackLeft:
                case Scenes.Side.BackRight:
                    PlayerDepth = 1;
                    break;
                case Scenes.Side.Left:
                case Scenes.Side.Center:
                case Scenes.Side.Right:
                case Scenes.Side.None:
                case Scenes.Side.Front:
                    PlayerDepth = 0;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }


        private void Update()
        {
            int moveY = GetMoveY();

            if (PlayerDepth <= 0 && moveY == -1) Scenes.Go(Scenes.Side.Front);
            if (PlayerDepth >= 1 && moveY == 1) Scenes.Go(Scenes.Side.Back);

            PlayerDepth += moveY * Time.deltaTime * PlayerSpeed * Player.transform.localScale.x;
            SetPlayerDirection(moveY);

            SetX(Player.gameObject, _halfWidth * ((-1 + 2 * PlayerDepth) * Destination));
            Player.transform.localScale = Vector3.one * (EntryScale + (DestinationScale - EntryScale) * PlayerDepth);
        }

        private void SetPlayerDirection(int moveY) =>
            Player.Direction = moveY == 0
                ? Player.WalkingDirection.Idle
                : moveY > 0
                    ? Player.WalkingDirection.Back
                    : Player.WalkingDirection.Front;

        private int GetMoveY() =>
            (Input.GetKey(KeyCode.UpArrow) ||
             Input.GetKey(Destination >= 0 ? KeyCode.RightArrow : KeyCode.LeftArrow)
                ? 1
                : 0)
            + (Input.GetKey(KeyCode.DownArrow) ||
               Input.GetKey(Destination >= 0 ? KeyCode.LeftArrow : KeyCode.RightArrow)
                ? -1
                : 0);

        private static void SetX(GameObject sprite, float x)
        {
            if (!sprite) return;
            sprite.transform.position = new Vector3(x, sprite.transform.position.y);
        }
    }
}