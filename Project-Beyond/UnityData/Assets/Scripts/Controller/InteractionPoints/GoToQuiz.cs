using System;
using UnityEngine;

namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// Loads the appropriate Quiz scene when the corresponding Minigame is completed (GameState.*QuizAllowed)
    /// And gives feedback after the quiz was failed/completed (see Awake method)
    /// </summary>
    public class GoToQuiz : GoToBackScene
    {
        public override KeyCode KeyCode => KeyCode.Space;

        public QuizTexts.Types Type;

        [Header("Speech")] public SpeechBubble SpeechBubble;
        public AudioClip NotAllowedClip;
        [Multiline(3)] public string NotAllowedText;
        public AudioClip AllowedClip;
        [Multiline(3)] public string AllowedText;
        public AudioClip SuccessClip;
        [Multiline(3)] public string SuccessText;
        public AudioClip FailureClip;
        [Multiline(3)] public string FailureText;

        public Animator Animator;

        private void Awake()
        {
            if (!GameState.QuizTried) return;
            GameState.QuizTried = false;
            if (Type == QuizTexts.Types.Desert && GameState.DesertQuizSucceeded 
                || Type == QuizTexts.Types.Space && GameState.SpaceQuizSucceeded
                || Type == QuizTexts.Types.Winter && GameState.WinterQuizSucceeded)
                SpeechBubble.Say(SuccessText, SuccessClip);
            else {
                SpeechBubble.Say(FailureText, FailureClip);
                Animator.Play("False");
            }
        }

        public override void DoInteraction(ParallaxController controller)
        {
            if (QuizTexts.IsQuizAllowed(Type)) {
                GameState.QuizTried = true;
                SpeechBubble.Say(AllowedText, AllowedClip, AllowedClip.length);
                Animator.Play("Hello");
                Invoke(nameof(ShowQuiz), AllowedClip.length);
            } else SpeechBubble.Say(NotAllowedText, NotAllowedClip, NotAllowedClip.length);
        }

        private void ShowQuiz() => Scenes.Go(Scenes.Side.Back);
    }
}