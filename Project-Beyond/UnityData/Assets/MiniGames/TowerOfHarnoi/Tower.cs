﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Beyond
{
    /// <summary>
    /// Contains the logic what brick is allowed to be placed on a tower and sets accordingly the bricks position
    /// </summary>
    public class Tower : MonoBehaviour
    {
        public List<Brick> bricks = new List<Brick>();
        public List<Transform> brickAncors = new List<Transform>();

        private int topIndex = -1;
        private HanoiLogic hanoiLogic;

        public void SetHanoiLogic(HanoiLogic hanoiLogic) => this.hanoiLogic = hanoiLogic;

        public bool AddBrick(Brick brick)
        {
            bool added = false;
            if (BrickSmallEnough(brick)) {
                bricks.Add(brick);
                topIndex++;
                SetBrickPossition(brick);

                added = true;
            }

            return added;
        }

        private void SetBrickPossition(Brick brick)
        {
            Vector3 newPosition = brickAncors[topIndex].position;
            brick.transform.position = newPosition;
        }

        public bool RemoveBrick(Brick brick)
        {
            bool removed = false;
            if (IsTop(brick)) {
                bricks.RemoveAt(topIndex);
                topIndex--;
                removed = true;
            }

            return removed;
        }

        private bool IsTop(Brick brick)
        {
            if (brick == bricks[topIndex])
                return true;
            return false;
        }

        private bool BrickSmallEnough(Brick brick)
        {
            if (topIndex == -1) {
                return true;
            }

            if (brick.GetBrickSize() < bricks[topIndex].GetBrickSize())
                return true;
            return false;
        }

    }
}