using System.Collections;
using UnityEngine;

namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// Triggers the "swoooosh" animation over the pit in winter2 scene, also see JumpLeftOverPit
    /// </summary>
    public class JumpRightOverPit : ParallaxInteractionPoint
    {
        public JumpLeftOverPit RightSide;
        public GameObject SpecialSprite;
        
        public override KeyCode KeyCode => KeyCode.RightArrow;
        public override bool PressedThisFrame => false;

        private void Awake()
        {
            SpecialSprite.SetActive(false);
        }

        // ReSharper disable once RedundantAssignment
        public override void DoInteraction(ParallaxController controller)
        {
            StartCoroutine(Flying(controller));
        }

        private IEnumerator Flying(ParallaxController controller)
        {
            controller.SetActive(false);
            controller.Player.SetActive(false);
            SpecialSprite.SetActive(true);

            AudioSource.PlayClipAtPoint(SoundEffects.Instance.Swoosh, SpecialSprite.transform.position);
            float speedPerSecond = (RightSide.PlayerX - PlayerX) / SoundEffects.Instance.Swoosh.length;
            while (controller.PlayerX < RightSide.PlayerX) {
                controller.PlayerX += Time.deltaTime * speedPerSecond;
                yield return null;
            }
            controller.PlayerX = RightSide.PlayerX;
            
            SpecialSprite.SetActive(false);
            controller.Player.SetActive(true);
            controller.SetActive(true);
        }
    }
}