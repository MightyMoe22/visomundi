﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager_Puzzle : MonoBehaviour
{
    public AudioClip Intro;
    public float volumeScale;

    private AudioSource audiosource;



    // Start is called before the first frame update
    void Start()
    {
        audiosource = GetComponent<AudioSource>();
        audiosource.clip = Intro;
        audiosource.Play();
        audiosource.loop = true;



    }
}



 

