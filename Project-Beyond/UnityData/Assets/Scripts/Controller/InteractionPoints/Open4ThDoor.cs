using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// Loads 5th ending when all amulets are in place & player stands in the right spot
    /// </summary>
    [RequireComponent(typeof(ParallaxController))]
    public class Open4ThDoor : MonoBehaviour
    {
        [Range(-1, 1)] public float PlayerX;
        [Range(-1, 1)] public float Range = 0.03f;
        public SpriteRenderer WhiteScreen;
        public int BlendingFrames = 30;

        private static bool _started;
        private ParallaxController _controller;

        private void Awake()
        {
            WhiteScreen.gameObject.SetActive(false);
            WhiteScreen.color = Color.clear;
            _controller = GetComponent<ParallaxController>();
        }

        private void Update()
        {
            if (!GameState.DesertAmuletApplied || !GameState.WinterAmuletApplied ||
                !GameState.SpaceAmuletApplied) return;
            if (!(Math.Abs(_controller.PlayerX - PlayerX) < Range) || _controller.PlayerLayer != 0) return;
            if (_started) return;
            _started = true;
            StartCoroutine(FadeWhiteAndGoTo4ThEnd());
        }

        private IEnumerator FadeWhiteAndGoTo4ThEnd()
        {
            _controller.SetActive(false);
            _controller.PlayerX = PlayerX;
            WhiteScreen.gameObject.SetActive(true);
            Timer.Pause(Time.time);
            for (float i = 0; i <= 1; i += 1f / BlendingFrames) {
                WhiteScreen.color = new Color(1, 1, 1, i);
                yield return null;
            }

            GameState.Choice = GameState.Result.Four;
            SceneManager.LoadScene(Scenes.Instance.EndingFour);
        }
    }
}