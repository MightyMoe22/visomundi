using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.U2D.TriangleNet;
using UnityEngine.SceneManagement;

namespace Beyond
{
    /// <summary>
    /// Singleton that holds all scene references and knows their arrangement
    /// Different Controllers then call Scenes.Go(Front|Back|Right|Left) to load neighbouring scenes
    ///
    /// Also, InitTimerAndGotoPantheon1 initializes the game
    /// </summary>
    [CreateAssetMenu]
    public class Scenes : ScriptableObject
    {
        #region Setup

        [Header("Halls")] [Scene] public string Pantheon1;
        [Scene] public string Pantheon2;
        [Scene] public string Pantheon3;
        [Scene] public string HallDesert;
        [Scene] public string HallWinter;
        [Scene] public string HallSpace;

        [Header("Winter")] [Scene] public string Winter1;
        [Scene] public string Winter2;
        [Scene] public string Winter3;
        [Scene] public string LockPicking;
        [Scene] public string HanoiTowers;
        [Scene] public string WinterQuiz;

        [Header("Space")] [Scene] public string Space1;
        [Scene] public string Space2;
        [Scene] public string SpaceBase;
        [Scene] public string ConnectingStars;
        [Scene] public string SpaceQuiz;

        [Header("Desert")] [Scene] public string Desert1;
        [Scene] public string Desert2;
        [Scene] public string Pyramid;
        [Scene] public string SlidePuzzle;
        [Scene] public string DesertQuiz;

        [Header("Videos")] [Scene] public string IntroVideo;
        [Scene] public string EndingStreet;
        [Scene] public string EndingDesert;
        [Scene] public string EndingSpace;
        [Scene] public string EndingWinter;
        [Scene] public string EndingFour;

        [Header("Other")] [Scene] public string Main;
        [Scene] public string ResultScreen;

        private readonly Dictionary<Tuple<string, Side>, Tuple<string, Side>> _connections =
            new Dictionary<Tuple<string, Side>, Tuple<string, Side>>();

        private void PopulateNeighbours()
        {
            AddConnection(Pantheon1, Side.Left, Side.Right, Pantheon2);
            AddConnection(Pantheon2, Side.BackLeft, Side.Front, HallSpace);
            AddConnection(Pantheon2, Side.Back, Side.Front, HallWinter);
            AddConnection(Pantheon2, Side.BackRight, Side.Front, HallDesert);
            AddConnection(Pantheon2, Side.Left, Side.Right, Pantheon3);

            AddConnection(HallWinter, Side.Back, Side.Front, Winter1);
            AddConnection(Winter1, Side.Back, Side.Front, Winter2);
            AddConnection(Winter2, Side.Back, Side.Front, Winter3);
            AddConnection(Winter2, Side.BackLeft, Side.Front, HanoiTowers);
            AddConnection(Winter3, Side.Back, Side.Front, WinterQuiz);

            AddConnection(HallSpace, Side.Back, Side.Front, Space1);
            AddConnection(Space1, Side.Back, Side.Front, SpaceQuiz);
            AddConnection(Space1, Side.Right, Side.Left, Space2);
            AddConnection(Space2, Side.Back, Side.Front, ConnectingStars);
            AddConnection(Space2, Side.Right, Side.Left, SpaceBase);
            AddConnection(SpaceBase, Side.Back, Side.Front, LockPicking);

            AddConnection(HallDesert, Side.Back, Side.Front, Desert1);
            AddConnection(Desert1, Side.Back, Side.Front, DesertQuiz);
            AddConnection(Desert1, Side.BackRight, Side.Front, SlidePuzzle);
            AddConnection(Desert1, Side.Right, Side.Left, Desert2);
            AddConnection(Desert2, Side.Back, Side.Left, Pyramid);
        }

        public enum Side
        {
            Left,
            Center,
            Right,
            Back,
            BackLeft,
            BackRight,
            Front,
            None
        }

        internal static Side EntrySide { get; set; } = Side.Center;

        #endregion

        #region Logic

        private static Scenes _instance;

        internal static Scenes Instance
        {
            get
            {
                if (_instance)
                    return _instance;
                _instance = Resources.Load<Scenes>("SceneReferences");
                _instance.PopulateNeighbours();
                return _instance;
            }
        }

        internal static void Go(Side side)
        {
            if (!Instance.GetNeighbourScene(SceneManager.GetActiveScene().name, side,
                    out string nextScene, out Side entrySide) || nextScene == null)
                return;
            EntrySide = entrySide;
            Cursor.visible = true;
            SceneManager.LoadScene(nextScene);
        }

        private void AddConnection(string scene1, Side side1, Side side2, string scene2)
        {
            if (scene1.Equals("") || scene2.Equals("")) {
                Debug.LogError("a scene reference in 'Resources/SceneReferences.asset' is not set");
                return;
            }

            Tuple<string, Side> pair1 = new Tuple<string, Side>(scene1, side1);
            Tuple<string, Side> pair2 = new Tuple<string, Side>(scene2, side2);
            _connections.Add(pair1, pair2);
            _connections.Add(pair2, pair1);
        }

        private bool GetNeighbourScene(string currentScene, Side side, out string nextScene, out Side entrySide)
        {
            Tuple<string, Side> key = new Tuple<string, Side>(currentScene, side);
            if (_connections.ContainsKey(key)) {
                nextScene = _connections[key].Item1;
                entrySide = _connections[key].Item2;
                return true;
            }

            nextScene = null;
            entrySide = Side.Center;
            return false;
        }

        #endregion

        public static void InitTimerAndGotoPantheon1()
        {
            EntrySide = Side.Center;
            Timer.SetValue(1200, Time.time);
            SceneManager.LoadScene(Instance.Pantheon1);
        }
    }
}