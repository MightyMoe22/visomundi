using System;
using UnityEngine;

namespace Beyond
{
    /// <summary>
    /// This class holds all static variables that represent game progress
    /// 
    /// ResetVariables() is called when returning to the main menu after completing / aborting the game
    /// NotifyInventory() triggers the ItemsChanged event, so that the Inventory Visualisation gets refreshed
    /// </summary>
    public static class GameState
    {
        public static bool WelcomeClipPlayed;

        public static bool QuizTried;

        public static bool DesertQuizAllowed;
        public static bool SpaceQuizAllowed;
        public static bool WinterQuizAllowed;

        public static bool DesertQuizSucceeded;
        public static bool SpaceQuizSucceeded;
        public static bool WinterQuizSucceeded;
        public static bool NoChoiceAvailable => !WinterQuizSucceeded && !DesertQuizSucceeded && !SpaceQuizSucceeded;

        public static bool PyramidOpened;

        public static bool PickAxe;
        public static bool BunsenBrenner;
        public static bool Chisel;

        public static bool DesertAmulet;
        public static bool DesertAmuletApplied;
        public static bool SpaceAmulet;
        public static bool SpaceAmuletAvailable;
        public static bool SpaceAmuletApplied;
        public static bool StoneOpened;
        public static bool WinterAmulet;
        public static bool WinterAmuletApplied;

        public static event Action ItemsChanged;
        public static void NotifyInventory() => ItemsChanged?.Invoke();

        public static Result Choice = Result.Tonne; 

        public enum Result
        {
            Tonne,
            Desert,
            Space,
            Winter,
            Four
        }

        public static void ResetVariables()
        {
            Timer.Pause(Time.time);
            SoundAndTimerSetup.Clear();
            WelcomeClipPlayed = QuizTried = DesertQuizAllowed = SpaceQuizAllowed = WinterQuizAllowed
                = DesertQuizSucceeded = SpaceQuizSucceeded = WinterQuizSucceeded = PyramidOpened = StoneOpened
                    = PickAxe = BunsenBrenner = Chisel = DesertAmulet = DesertAmuletApplied = SpaceAmulet 
                        = SpaceAmuletApplied = SpaceAmuletAvailable = WinterAmulet = WinterAmuletApplied = false;
            Choice = Result.Tonne;
            Pantheon1OverlayShown = Pantheon2OverlayShown = ForceChooseEndingSubscribed
                = PlayedWelcomeDesert = PlayedWelcomeSpace = PlayedWelcomeWinter = CheatsEnabled = false;
            AmuletsFound = 0;
        }

        public static bool Pantheon1OverlayShown;
        public static bool Pantheon2OverlayShown;
        public static bool ForceChooseEndingSubscribed;
        public static bool PlayedWelcomeDesert;
        public static bool PlayedWelcomeSpace;
        public static bool PlayedWelcomeWinter;
        public static int AmuletsFound = 0;
        public static bool CheatsEnabled;
    }
}