using UnityEngine;

namespace Beyond
{
    /// <inheritdoc />
    /// <summary>
    /// Allows Picking up a Item (Pickaxe / Chisel / WinterAmulet)
    /// </summary>
    public abstract class PickupItem : ParallaxInteractionPoint
    {
        [Multiline(2)] public string PickupText;
        public GameObject Item;

        private void Awake()
        {
            if (Item != null) Item.SetActive(!ItemPickedUp);
        }

        public override void DoInteraction(ParallaxController controller)
        {
            if (Item == null || ItemPickedUp) return;
            Item.SetActive(false);
            ItemPickedUp = true;
            AudioSource.PlayClipAtPoint(SoundEffects.Instance.PickupItem, Item.transform.position);
            SaySth(controller);
        }

        protected virtual void SaySth(ParallaxController controller) =>
            controller.Player.SpeechBubble.Say(PickupText, null);

        protected override bool AdditionalRequirement() => !ItemPickedUp;

        protected abstract bool ItemPickedUp { get; set; }
    }
}