﻿using System.Collections;
using System.Collections.Generic;
using Beyond;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/* LevelManager verwaltet Ablauf und Logik des Levels */

public class LevelManager : MonoBehaviour
{
    public static bool gameStarted = false;
    public GameObject[] Levels;
    public Level currentLevel;
    public int clicksLeft;
    public Text clicksLeftText, subtext;
    public Image levelPreview;
    public GameObject TutorialOverlay;

    private int progressCount = 0;
    private int levelIndex;
    private GameObject currentLevelObject;
    



    // Start is called before the first frame update
    void Start()
    {
        //UI
        clicksLeftText.text = string.Empty;
        subtext.gameObject.SetActive(false);


        setupLevel(0);
        currentLevelObject.SetActive(false);

        gameStarted = false;
        StartCoroutine(TutorialCoroutine());
        
        
        
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.DownArrow)) Scenes.Go(Scenes.Side.Front);
    }


    //wird aufgerufen, wenn richtiger Stern angeklickt und Überprüfung, ob Level geschafft
    public void validStarSelected()
    {
        progressCount++;
        Debug.Log("CurrentNumber:" + progressCount);

        if(progressCount == currentLevel.starcount)
        {
            FindObjectOfType<AudioManager>().SternbildRichtigSound();
            StartCoroutine(LevelComplete(true));
        }
    }


    //MaxClicks werden festgelegt und die Methode zum Sprite einblenden aufgerufen
    public void startLevel()
    {
        clicksLeft = currentLevel.maxClicks;
        StartCoroutine(ShowSprite());
    }


    //richtiges Level wird geladen, erzeugt und referenziert
    public void setupLevel(int index)
    {
        
        levelPreview.gameObject.SetActive(false);
        levelIndex = index;

        currentLevelObject = Instantiate(Levels[index], Vector3.zero, Quaternion.identity);
        currentLevel = currentLevelObject.GetComponent<Level>();
        currentLevel.ResetLevel();
    }


    // Gameobjects werden resettet und LevelIndex wird erhöht und setupLevel und startLevel Aufruf
    private IEnumerator LevelComplete(bool successfull)
    {
        FindObjectOfType<LineManager>().LevelComplete(successfull);
        yield return new WaitForSeconds(1f);
        Destroy(currentLevelObject);
        Destroy(currentLevel);
        FindObjectOfType<LineManager>().LineReset();
        progressCount = 0;

        clicksLeftText.text = string.Empty;
        subtext.gameObject.SetActive(false);


        if (levelIndex + 1 < Levels.Length && successfull)
        {
            levelIndex++;
            setupLevel(levelIndex);
            FindObjectOfType<InputManager>().LevelReset();
            startLevel();

        }
        else
        {
            GameFinished(successfull);
        }
        
    }


    // Die Lösung vom momentanen Level wird eingeblendet und langsam ausgeblendet
    private IEnumerator ShowSprite()
    {
        levelPreview.gameObject.SetActive(true);
        levelPreview.color = Color.clear;
        levelPreview.sprite = currentLevel.levelSprite;

        int frames = (int)(1f / Time.deltaTime);
        Color lerpedColor = Color.clear;
        Color startColor = new Color(1, 1, 1, 0);

        //Einfaden
        for(int i = 0; i < frames; i++)
        {
            lerpedColor = Color.Lerp(startColor, Color.white, (float)i / frames);
            levelPreview.color = lerpedColor;
            yield return null;
        }

        yield return new WaitForSeconds(3f);

        //Ausfaden
        for (int i = 0; i < frames; i++)
        {
            lerpedColor = Color.Lerp(Color.white, startColor, (float)i / frames);
            levelPreview.color = lerpedColor;
            yield return null;
        }

        levelPreview.gameObject.SetActive(false);

        //UI
        subtext.gameObject.SetActive(true);
        clicksLeftText.text = clicksLeft.ToString();
    }


    // Erlaubte Klicks wird reduziert
    public void decreaseClicks()
    {
        
        clicksLeft--;
        if(!isClickAllowed())
        {
            FindObjectOfType<AudioManager>().SternbildFalschSound();
            StartCoroutine(LevelComplete(false));
        }

        //UI
        clicksLeftText.text = clicksLeft.ToString();
    }


    //Überprüfung ob erlaubte Klicks größer Null
    public bool isClickAllowed()
    {
        return clicksLeft >= 0;
    }

    //Hauptspiel wird an entsprechender Stelle fortgesetzt
    public void GameFinished(bool successfull)
    {
        if (successfull) {
            GameState.SpaceQuizAllowed = true;
            GameState.NotifyInventory();
        }
        Scenes.Go(Scenes.Side.Front);
    }


    //Tutorial standarmäßig am Anfang da. Wenn Mausklick erfolgt, dann wird Tutorial ausgeblendet.
    public IEnumerator TutorialCoroutine()
    {
        while (!Input.GetMouseButtonDown(0))
        {
            yield return null;
        }

        TutorialOverlay.SetActive(false);
        gameStarted = true;

        startLevel();
        currentLevelObject.SetActive(true);
    }

}
