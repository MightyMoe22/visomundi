using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

namespace Beyond
{
    /// <summary>
    /// Loads the next scene when the video is finished (skip with S or space)
    /// </summary>
    [RequireComponent(typeof(VideoPlayer))]
    public class NextScene : MonoBehaviour
    {
        public enum Next
        {
            Pantheon, Results
        }

        public Next TheNextScene;
        private VideoPlayer _videoPlayer;

        private void Awake()
        {
            GetComponent<VideoPlayer>().loopPointReached += source => GotoNextScene();
        }

        private void GotoNextScene()
        {
            switch (TheNextScene) {
                case Next.Pantheon:
                    Scenes.InitTimerAndGotoPantheon1();
                    break;
                case Next.Results:
                    SceneManager.LoadScene(Scenes.Instance.ResultScreen);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.Space)) GotoNextScene();
        }
    }
}