using System;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Serialization;

namespace Beyond
{
    /// <summary>
    /// Singleton, that holds backgroundmusic references, also AudioMixerSnapshots are used for blending between different soundscapes
    /// </summary>
    [CreateAssetMenu]
    public class SoundScapes : ScriptableObject
    {
        [FormerlySerializedAs("Halls")] [Header("Audio Clips")]
        public AudioClip HallsClip;
        [FormerlySerializedAs("Desert")] public AudioClip DesertClip;
        [FormerlySerializedAs("Space")] public AudioClip SpaceClip;
        [FormerlySerializedAs("Winter")] public AudioClip WinterClip;
        
        [Header("Audio Mixer Groups")]
        public AudioMixerGroup HallsGroup;
        public AudioMixerGroup DesertGroup;
        public AudioMixerGroup SpaceGroup;
        public AudioMixerGroup WinterGroup;

        [Header("Audio Mixer Snapshots")]
        public AudioMixerSnapshot HallsSnapshot;
        public AudioMixerSnapshot DesertSnapshot;
        public AudioMixerSnapshot SpaceSnapshot;
        public AudioMixerSnapshot WinterSnapshot;
        public AudioMixerSnapshot MiniGameBackgroundMusicSnapshot;
        public float TransitionTime = 5;

        public enum Setting
        {
            Halls,
            Desert,
            Space,
            Winter,
            Minigame
        }

        public static SoundScapes Instance => _instance
            ? _instance
            : _instance = Resources.Load<SoundScapes>("SoundScapes");

        private static SoundScapes _instance;

        public static AudioMixerSnapshot GetSnapshot(Setting setting)
        {
            switch (setting) {
                case Setting.Halls:
                    return Instance.HallsSnapshot;
                case Setting.Desert:
                    return Instance.DesertSnapshot;
                case Setting.Space:
                    return Instance.SpaceSnapshot;
                case Setting.Winter:
                    return Instance.WinterSnapshot;
                case Setting.Minigame:
                    return Instance.MiniGameBackgroundMusicSnapshot;
                default:
                    throw new ArgumentOutOfRangeException(nameof(setting), setting, null);
            }
        }
    }
}